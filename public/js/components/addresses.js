new Vue({
    el: '#addresses',
    props: ['exist'],
    data: {
        itemsRemove: [],
        address_error: 0,
        zip_code: '',
        street: '',
        street_number: '',
        district: '',
        city: '',
        state: '',
        country: '',
        geo_location: '',
        addresses: []
    },
    methods: {
        fillFields: function (e) {
            this.street = e.logradouro;
            this.district = e.bairro;
            this.city = e.cidade;
            this.state = e.estado_info.nome;
            this.country = 'Brasil';
            this.geo_location = JSON.stringify(e);
        },
        findByCep: function (cep) {
            $.get('http://api.postmon.com.br/v1/cep/' + cep, this.fillFields);
        },
        get_current_address: function () {
            var $return = {
                zip_code: this.zip_code,
                street: this.street,
                street_number: this.street_number,
                district: this.district,
                city: this.city,
                state: this.state,
                country: this.country,
                geo_location: this.geo_location
            };
            $return['toJson'] = JSON.stringify($return);
            return $return;
        },
        clear_fields: function () {
            this.zip_code = '';
            this.street = '';
            this.street_number = '';
            this.district = '';
            this.city = '';
            this.state = '';
            this.country = '';
            this.geo_location = '';
        },
        validate: function (e) {
            e.preventDefault();
            var self = this;
            $.ajax({
                url: Laravel.url + '/api/zip-validate',
                data: self.get_current_address(),
                method: 'post',
                dataType: 'json',
                success: function (r) {
                    self.address_error = 0;
                    self.save();
                },
                error: function (xhr, xhrResponse) {
                    self.address_error = xhr.responseJSON;
                    goToByScroll($('#address-error'))
                }
            });
        },
        edit: function (e, key) {
            e.preventDefault()
            this.zip_code = this.addresses[key].zip_code;
            this.street_number = this.addresses[key].street_number;
            this.fillFields(JSON.parse(this.addresses[key].geo_location));
            this.addresses.$remove(this.addresses[key]);
        },
        destroy: function(e, key) {
            e.preventDefault();
            if (this.exist != '') {
                this.itemsRemove.push(this.addresses[key].id);
            }
            this.addresses.$remove(this.addresses[key]);
        },
        save: function () {
            this.addresses.push(this.get_current_address());
            this.clear_fields();
        }
    },
    watch: {
        'zip_code': function (n, o) {
            n = n.replace(/\D/g, '');
            if (/^[0-9]{8}$/.test(n)) {
                this.findByCep(n);
            }
        }
    },
    beforeCompile: function () {
        if (this.exist != '') {
            var exist = JSON.parse(this.exist);
            Object.keys(exist).map(function(k, o){
                exist[o]['toJson'] = JSON.stringify(exist[o]);
                this.addresses.push(exist[o]);
            }.bind(this));
            // this.addresses = ;
        }
    }
});