new Vue({
    el: '#environment',
    props: ['languages'],
    data: {
        itemsRemove: [],
        uploads: [],
        reference: '',
        name: {},
        color: '',
        upload: ''
    },
    methods: {
        save_environment: function (e) {
            e.preventDefault();
            if (this.reference == '' || this.color == '') {
                toastr.error('Os campos com (*) são campos obrigatórios');
                return false;
            }
            this.uploads.push({name: this.name, color: this.color, upload: this.upload, reference: this.reference});
            this.clear_or_fill();
        },
        save: function (e) {
            var self = this;
            var form = new FormData();
            form.append('file', e.target.files[0]);
            form.append('path', 'uploads/tmp-image/' + Laravel.path + '/');
            $.ajax({
                url: Laravel.url + '/api/tmp-image',
                method: 'post',
                cache: false,
                data: form,
                contentType: false,
                processData: false,
                success: function (r) {
                    if (r.error == true)
                        toastr.error(r.msg);
                    else {
                        self.upload = r.data;
                    }
                }
            });
        },
        clear_or_fill: function (fields, edit) {
            this.name = fields ? fields.name : this.read_languages();
            this.color = fields ? fields.color : '';
            this.upload = fields ? fields.upload : '';
            this.reference = fields ? fields.reference : '';
            $('#upload-clear').val('');
        },
        edit: function (key, e) {
            e.preventDefault();
            var current = this.uploads[key];
            this.clear_or_fill(current);
            this.uploads.$remove(current);
        },
        edit_full: function(key, ref, e) {
            e.preventDefault();
            console.log($(e).data());
            if ($('#edit-' + ref).length) {
                var fields = JSON.parse($('#edit-' + ref).val());
                this.name = JSON.parse(fields.name);
                this.color = fields.color;
                this.upload = JSON.parse(fields.upload);
                this.reference = fields.reference;
            }
            $('#'+ref).remove();
        },
        destroy: function (key, e) {
            e.preventDefault();
            var current = this.uploads[key];
            if (current.id)
                this.itemsRemove.push(current.id);
            this.uploads.$remove(current);
        },
        destroy_full: function(key, ref, e) {
            e.preventDefault();
            console.log(key);
            this.itemsRemove.push(key);
            $('#'+ref).remove();
        },
        read_languages: function () {
            if (this.languages) {
                var self = this;
                self.$set('name',{});
                JSON.parse(this.languages).map(function(o, k){
                    self.$set('name.' + o.slug, '');
                });
            }
        },
        get_string: function(object) {
            return JSON.stringify(object);
        }
    },
    beforeCompile: function () {
        if (this.languages) {
            var self = this;
            JSON.parse(this.languages).map(function(o, k){
                self.$set('name.' + o.slug, '');
            });
        }
    }
});