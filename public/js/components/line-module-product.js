function guid() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }

    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
        s4() + '-' + s4() + s4() + s4();
}
function select(value, key, object, returnObject) {
    var $return = false;
    Object.keys(object).map(function (k, r) {
        if (object[k][key] == value) {
            $return = returnObject ? JSON.parse(JSON.stringify(object[k])) : k;
        }
    });
    return $return;
}
new Vue({
    el: 'body',
    props: ['languages'],
    data: {
        env: {
            id: '',
            color_id: '',
            name: {},
            reference: '',
            upload: {},
            rows: [],
            itemsRemove: [],
            wait: false
        },
        module: {
            itemsRemove: [],
            id: '',
            wait: false,
            name: {},
            environment: '',
            width: '',
            height: '',
            descriptions: {},
            depth: '',
            color_id: '',
            reference: '',
            upload: {},
            rows: [],
            row_modules: []
        }
    },
    methods: {
        e_validate: function () {
            if (this.env.reference == '') {
                toastr.error('Para adicionar o ambiente, preencha o campo de referência');
                return false;
            }
            if (this.env.color_id == '') {
                toastr.error('Para adicionar o ambiente, selecione a Cor.');
                return false;
            }
            if (!Object.keys(this.env.upload).length) {
                toastr.error('Para adicionar o ambiente, adicione uma imagem.');
                return false;
            }
            return true;
        },
        e_save: function (e) {
            e.preventDefault();
            if (this.env.wait == false) {
                if (this.e_validate()) {
                    this.env.rows.push({
                        id: guid(),
                        color_id: this.env.color_id,
                        name: this.env.name,
                        reference: this.env.reference,
                        upload: this.env.upload
                    });
                    this.e_clear_fields();
                }
            }
        },
        e_clear_fields: function () {
            this.env.id = '';
            this.env.color_id = '';
            this.env.name = {};
            this.env.reference = '';
            this.env.upload = {};
            $('#upload-clear').val('');
        },
        e_show_name: function (data) {
            var $html = '';
            Object.keys(data).map(function (k, o) {
                $html += data[k] + ', ';
            });
            return $html;
        },
        e_show_color: function (id) {
            return $('#color').find('[value="' + id + '"]').text();
        },
        e_toString: function (o) {
            return JSON.stringify(o);
        },
        e_edit: function (e, id, edit) {
            e.preventDefault();
            var o;
            if (edit) {
                var object = JSON.parse($('[name^="environments"]').val());
                o = select(id, 'id', [object], true);
            } else {
                o = this.env.rows[select(id, 'id', this.env.rows, false)];
            }

            this.env.id = o.id;
            this.env.name = o.name;
            this.env.color_id = o.color_id;
            this.env.reference = o.reference;
            this.env.upload = o.upload;

            if (!edit)
                this.env.rows.$remove(o);
            else {
                $('#' + id).remove();
            }
        },
        e_destroy: function (e, id, edit) {
            e.preventDefault();

            if (!edit) {
                var o = this.env.rows[select(id, 'id', this.env.rows, false)];
                this.env.itemsRemove.push(o.id);
                this.env.rows.$remove(o);
            } else {
                $('#' + id).remove();
            }
            this.env.itemsRemove.push(id);
        },
        e_tmp_image: function (e) {
            var self = this;
            self.env.wait = true;
            var form = new FormData();
            form.append('file', e.target.files[0]);
            form.append('path', 'uploads/tmp-image/');
            $.ajax({
                url: Laravel.url + '/api/tmp-image',
                method: 'post',
                cache: false,
                data: form,
                contentType: false,
                processData: false,
                success: function (r) {
                    if (r.error == true) {
                        self.env.wait = false;
                        toastr.error(r.msg);
                    } else {
                        self.env.upload = r.data;
                        self.env.wait = false;
                        toastr.success(r.msg);
                    }
                }
            });
        },
        m_validate: function () {
            var $return = true;

            if (this.module.color_id == '' || this.module.reference == '' || this.module.upload) {
                toastr.error('Os campos com * são obrigatórios para cadastrar a variação.');
                $return = false;
            }

            return $return;
        },
        m_save_module: function (e) {
            e.preventDefault();
            if (this.m_validate()) {
                this.module.row_modules.push({
                    id: guid(),
                    name: this.module.name,
                    descriptions: this.module.descriptions,
                    environment: this.module.environment,
                    reference: this.module.reference,
                    width: this.module.width,
                    height: this.module.height,
                    depth: this.module.depth,
                    grids: this.module.rows
                });

                this.module.id = '';
                this.module.name = {};
                this.module.environment = '';
                this.module.descriptions = {};
                this.module.reference = '';
                this.module.width = '';
                this.module.height = '';
                this.module.depth = '';
                this.module.rows = [];
            }
        },
        m_save_validate: function() {
            if (this.module.reference == '') {
                toastr.error('Para adicionar a variação é necessário preencher a Referência.');
                return false;
            }
            if (this.module.color_id == '') {
                toastr.error('Para adicionar a variação do módulo é necessário selecionar a Cor.');
                return false;
            }
            if (Object.keys(this.module.upload).length) {
                toastr.error('Para adicionar a variação, selecione uma imagem.');
                return false;
            }
            return true;
        },
        m_save: function (e) {
            e.preventDefault();
            if (this.m_save_validate() && this.module.wait == false) {
                this.module.rows.push({
                    id: guid(),
                    color_id: this.module.color_id,
                    reference: this.module.reference,
                    upload: this.module.upload
                });
                this.m_clear_fields();
            }
        },
        m_tmp_image: function (e) {
            var self = this;
            this.module.wait = true;
            var form = new FormData();
            form.append('file', e.target.files[0]);
            form.append('path', 'uploads/tmp-image/');
            $.ajax({
                url: Laravel.url + '/api/tmp-image',
                method: 'post',
                cache: false,
                data: form,
                contentType: false,
                processData: false,
                success: function (r) {
                    if (r.error == true)
                        toastr.error(r.msg);
                    else {
                        self.module.upload = r.data;
                        toastr.success(r.msg);
                    }
                },
                complete: function () {
                    self.module.wait = false;
                }
            });
        },
        m_clear_fields: function () {
            this.module.id = '';
            this.module.color_id = '';
            this.module.reference = '';
            this.module.upload = {};
            $('#upload-clear').val('');
        },
        m_toString: function (o) {
            return JSON.stringify(o);
        },
        m_show_color: function (id) {
            if (id == '')
                return '';
            return $('#color').find('[value="' + id + '"]').text();
        },
        m_show_name: function (data) {
            var $html = '';
            Object.keys(data).map(function (k, o) {
                $html += data[k] + ', ';
            });
            return $html;
        },
        m_show_environment: function (id) {
            if (id == '')
                return '';
            return $('#environment').find('[value="' + id + '"]').text();
        },
        m_edit: function (e, id, edit) {
            e.preventDefault();
            var o;
            if (edit) {
                var object = JSON.parse($('[name^="grids"]').val());
                o = select(id, 'id', [object], true);
            } else {
                o = this.module.rows[select(id, 'id', this.module.rows, false)];
            }

            this.module.id = o.id;
            this.module.color_id = o.color_id;
            this.module.name = o.name;
            this.module.reference = o.reference;
            this.module.upload = o.upload;

            if (!edit)
                this.module.rows.$remove(o);
            else {
                $('#' + id).remove();
            }
        },
        m_destroy: function (e, id) {
            e.preventDefault();

            var o = this.module.rows[select(id, 'id', this.module.rows, false)];
            this.module.itemsRemove.push(id);
            this.module.rows.$remove(o);
        }
    }
});