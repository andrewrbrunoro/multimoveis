$(document).ready(function(){
    $('.delete').on('submit', function (e) {
        e.preventDefault();
        swal({
            title: 'Deseja continuar?',
            showCancelButton: true,
            confirmButtonText: 'Continuar',
            showLoaderOnConfirm: true,
            allowOutsideClick: false
        }, function(r) {
            if (r) {
                this.submit();
            }
        }.bind(this));
    });
});