new Vue({
    el: '#course-attributes',
    props: ['count'],
    data: {
        current: 0,
        tabs: [],
        new_tab: '',
        itemsRemove: []
    },
    methods: {
        change_tab: function (key) {
            this.current = key;
        },
        save_tab: function () {
            if (this.new_tab != '') {
                this.tabs.push({title: this.new_tab, key: slugify(this.new_tab), tab_index: parseInt(this.count) + 1});
                toastr.info('A TAB ' + this.new_tab + ', foi adicionada.');
                this.new_tab = '';
            }
        },
        close_modal: function () {
            $('.textarea_editor').summernote({height: 400});
        },
        destroy: function (key, e) {
            e.preventDefault();
            var self = this;
            swal({
                title: 'Você tem certeza?',
                text: 'Ao deletar, você pode não irá conseguir recuperar o conteúdo.',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Continuar'
            }, function (r) {
                if (r) {
                    self.itemsRemove.push(key);
                    $('#tab-' + key).remove();
                    $('#course-attributes-' + key).remove();
                }
            })
        }
    },
    ready: function () {
        $('textarea').summernote({height: 400});
    }
});