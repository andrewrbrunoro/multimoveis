function goToByScroll(jQueryElement) {
    $('html,body').animate({scrollTop: jQueryElement.offset().top - 200}, 'slow');
}

function slugify(text, replaceTo) {
    replaceTo = replaceTo ? replaceTo : '-';
    var text = text.toString().toLowerCase()
        .replace(/\s+/g, replaceTo)           // Replace spaces with -
        .replace(/[^\w\-]+/g, replaceTo)       // Remove all non-word chars
        .replace(/\-\-+/g, replaceTo)         // Replace multiple - with single -
        .replace(/^-+/, replaceTo)             // Trim - from start of text
        .replace(/-+$/, replaceTo);            // Trim - from end of text
    return text.replace('-', '');
}