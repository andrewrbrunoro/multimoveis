$(document).ready( function() {
    var modal = $('.fd_modal');
    var overlay = $('.fd_overlay');
    var close_modal = $('.close-modal');

    $('#carousel-internas').carousel({
        interval: 5000,
    });

    $('#carousel-modal').carousel({
        interval: false,
    });

    $('.owl-carousel a.item, .lupa, .open-video').click( function() {
        modal.delay(150).fadeIn(300);
        overlay.fadeIn(300);
    });

    $('.close-modal, .fd_overlay').click( function() {
        $(overlay).delay(150).fadeOut(300);
        $(modal).fadeOut(300);
    });


    $("#zoom_05").elevateZoom({
        constrainType:"height",
        constrainSize:274,
        zoomType: "lens",
        containLensZoom: true,
        gallery:'gallery_01',
        cursor: 'pointer',
        galleryActiveClass: "active"
    });

    $("#carousel-modal .carousel-inner .item img").elevateZoom({
        constrainType:"height",
        zoomType: "lens",
        containLensZoom: true,
        cursor: 'pointer',
    });


    $('#owl-carousel').owlCarousel({
        center: true,
        stagePadding: 0,
        items: 1,
        loop: true,
        margin: 10,
        nav: true,
        navText: ['<img src="/assets/images/icons/arrow-left.png" alt="<<">', '<img src="/assets/images/icons/arrow-right.png" alt=">>">'],

        thumbs: true,
        thumbImage: true,
        thumbContainerClass: 'owl-thumbs',
        thumbItemClass: 'owl-thumb-item',

        responsive: {
            0: {
                items: 1
            }
        },
    });
    $('.owl-thumb-item img').after('<div class="shadow"></div>');


});
