<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
if (isset($router)) {

    $router->get('auth', 'Auth\LoginController@login')->name('login');
    $router->post('auth', 'Auth\LoginController@login')->name('auth');
    $router->get('logout', 'Auth\LoginController@logout')->name('logout');

    $router->get('/', 'Site\HomeController@index')->name('front.index');
    $router->get('linha/{slug}', 'Site\HomeController@show_line')->name('linha.show');
    $router->get('contato', 'Site\ContactController@index')->name('contact.index');
    $router->post('contato', 'Site\ContactController@store')->name('contact.store');
    $router->get('a-empresa', 'Site\HomeController@about')->name('about.index');
    $router->get('video-institucional', 'Site\HomeController@video')->name('video.index');

    $router->group(['middleware' => 'auth', 'prefix' => 'admin'], function () use ($router) {
        $router->get('/', function () {
            return redirect()->route('line.index');
        });
        $router->get('/panel', function () {
            return redirect()->route('line.index');
        })->name('dashboard');

        $router->resource('user', 'Admin\UserController');

        //Multi Móveis
        $router->resource('line', 'Admin\LineController');
        $router->resource('lead', 'Admin\LeadController', [
            'except' => ['create', 'edit', 'destroy']
        ]);

        $router->resource('product', 'Admin\ProductController');
        $router->resource('blog', 'Admin\BlogController');

        $router->resource('environment', 'Admin\EnvironmentController');
    });

}