<?php
return [

    'error_create' => 'Não foi possível criar :msg, tente novamente, caso o erro persista tente novamente mais tarde.',
    'error_update' => 'Não foi possível atualizar :msg, tente novamente, caso o erro persista tente novamente mais tarde.',
    'error_destroy' => 'Não foi possível deletar :msg',

    'success_create' => ':msg foi criado, <a href=":url" class="btn btn-xs btn-info"><i class="fa fa-pencil"></i> clique aqui</a> para editar.',
    'success_update' => ':msg foi atualizado.',
    'success_destroy' => ':msg foi deletedo.',

    'not_exist' => 'Registro não encontrado.'
];