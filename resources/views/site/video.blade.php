@extends('front-interna')

@section('content')
    <style>
        h2, label {
            color: #FFF;
        }
        p {
            color: #FFF;
        }
    </style>
    <main class="container">
        @include('site.elements.header-front')
        <section class="home">
            <h2>Vídeo Institucional</h2>
            <p>
                Confira abaixo um pouco mais sobre nossa empresa.
            </p>

            <iframe width="500" height="480" src="https://www.youtube.com/embed/dfYRGu0L-z0" frameborder="0" allowfullscreen></iframe>
        </section>
    </main>
    <div class="clearfix"></div>

    @include('site.elements.footer-show')

@endsection