@extends('front')

@section('content')

    <main class="container">
        @include('site.elements.header-front')
        <section class="home">
            @foreach($lines as $line)
                @php
                    $environment = $line->Environment->first();
                @endphp
                <div class="row">
                    <a href="{!! route('linha.show', $line->slug) !!}" class="col-xs-12 item baby">
                        <figure class="fundo">
                            <img src="{!! url('/assets/environment/'. $environment->upload_json->home) !!}" width="437" height="218" alt="Multimóveis Baby">
                        </figure>
                        <div class="white-box">
                            <figure>
                                {!! str_limit($line->descriptions_json['portugues'], 150) !!}
                            </figure>
                        </div>
                    </a>
                </div>
            @endforeach
        </section>
        <div class="clearfix"></div>
        <section class="bottom">
            <address>
                MULTIMÓVEIS<br />
                Rua Carlos Dreher Neto, 1918 - Caixa Postal 639<br />
                Bento Gonçalves - RS - Brasil<br />
                CEP 95706-44<br />
                54 2102 4000 - <a href="mailto:multimoveis@multimoveis.com">multimoveis@multimoveis.com</a>
            </address>
        </section>
    </main>

@endsection