@extends('front-interna')

@section('content')
    <style>
        h2, label {
            color: #FFF;
        }
        p {
            color: #FFF;
        }
    </style>
    <main class="container">
        @include('site.elements.header-front')
        <section class="home">
            <div class="container2">
                <h2>Quem Somos</h2>
                <p class="p1">
                    A Multimóveis, fundada em 1995, na Serra Gaúcha, está localizada no maior pólo moveleiro
                    do Brasil. Com a missão de oferecer bem estar em mobiliário, de forma criativa, flexível
                    e rentável, em harmonia com o ser humano, a sociedade e o meio ambiente, a empresa produz
                    móveis de qualidade e com design arrojado respeitando as diferentes culturas mundiais e
                    aos exigentes consumidores deste mundo moderno.
                </p>

                <p class="p3">
                    Utilizando tecnologia de ponta, os produtos da Multimóveis Baby, Multimóveis Decor, Multipla
                    e Eko Ambientes se diferenciam através do acabamento superior, demonstrando o cuidado e
                    carinho na fabricação dos móveis. Nosso desafio é ser referência em mobiliário para o bem
                    estar das pessoas, oferecendo aos clientes soluções práticas e facilidades, com foco nos
                    mais altos padrões em serviços, qualidade e acabamentos disponíveis no mercado. Para a
                    Multimóveis, o cliente é a prioridade das suas ações, e a equipe seu maior patrimônio.
                    De bem com a vida, mais do que um slogan: uma filosofia de trabalho e de vida.
                </p>
                <h2>Histórico</h2>

                <p class="p3">
                    Fundada em 01 de janeiro de 1995, fruto da união dos objetivos comuns, sonhos e muito
                    trabalho de seus sócios diretores Ivo Cusin, Euclides Longhi e Maristela Cusin Longhi,
                    a Multimóveis Indústria de Móveis, localizada em Bento Gonçalves, Rio Grande do Sul,
                    iniciou suas atividades fabricando móveis infantis e em pouco tempo tornou-se uma
                    das maiores fábricas especializadas no segmento.
                </p>

                <p class="p3">
                    Como estratégia de expansão e visando uma maior atuação no mercado nacional e internacional,
                    em 1998 é adquirida a Decormóvel Indústria de Móveis, fabricante de racks, estantes,
                    escrivaninhas e dormitórios. Após quatro anos ela foi  incorporada à Multimóveis, dando
                    origem a nova identidade visual da empresa. Em 2009, buscando ampliar seu nicho de mercado,
                    a Multimóveis lançou a EKO Ambientes, desenvolvida para buscar um espaço diferenciado no
                    mercado de móveis planejados. Já em 2011 surge a Multipla, marca de móveis modulados
                    lançada para atender um público que busca uma casa bem decorada, com design moderdo aliado
                    a preços acessíveis. Com as novas propostas de produtos que surgiram ao longo dos anos, em
                    2012 a Multimóveis modernizou sua logomarca, deixando-a mais dinâmica e corporativa para
                    que pudesse abranger as demais marcas que surgiram na empresa.
                </p>

                <p class="p3">
                    Atualmente, com uma área construída de 22.500m², a Multimóveis conta com 280 funcionários
                    e alia equipamentos de última geração à capacitação profissional de sua equipe. Além do
                    mercado brasileiro, os produtos são aceitos mundialmente, sendo exportados para mais de 40 países.
                </p>


                <h2>NEGÓCIO</h2>

                <p class="p3">Bem estar em mobiliário.</p>


                <h2>MISSÃO</h2>
                <p class="p3">
                    Oferecer bem estar em mobiliário, de forma flexível, criativa e rentável, em harmonia
                    com o ser humano, a sociedade e o meio ambiente.
                </p>

                <h2>PRINCÍPIOS</h2>
                <p class="p3">
                    Cliente: nossa prioridade.
                    <br>Responsabilidade: envolvimento e comprometimento de todos.
                    <br>Equipe: o grande diferencial.
                    <br>Rentabilidade: garantia de continuidade e crescimento.
                </p>

                <h2>VISÃO</h2>
                <p class="p3">Ser referência em mobiliário para o bem estar das pessoas.</p>
            </div>
        </section>
    </main>
    <div class="clearfix"></div>

    @include('site.elements.footer-show')

@endsection