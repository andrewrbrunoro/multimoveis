@extends('front-interna')

@section('content')
    @php
        $environment = $show->Environment->first();
    @endphp


    <main class="internas baby">
        {{--<figure class="logo-interna">--}}
            {{--<img src="{!! url('/assets/images/multimoveis-baby-logo-interna.png') !!}" alt="">--}}
        {{--</figure>--}}
        <ul class="list-inline pull-right">
            <li><a href="#"><img src="{!! url('/assets/images/icons/blog-internas.png') !!}" alt=""></a></li>
            <li><a href="#"><img src="{!! url('/assets/images/icons/facebook-internas.png') !!}" alt=""></a></li>
            <li><a href="#"><img src="{!! url('/assets/images/icons/instagram-internas.png') !!}" alt=""></a></li>
            <li>
                <form class="" action="" method="post">
                    <input type="text" name="search_interna" id="search_interna" value="" placeholder="BUSCA" required>
                </form>
            </li>
        </ul>
        <div class="row">
            <div class="col-xs-12">
                <div id="carousel-internas" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        @foreach($show->Environment as $key => $env)
                        <li data-target="#carousel-internas" data-slide-to="{!! $key !!}" class="active"></li>
                        @endforeach
                    </ol>

                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox">
                        @foreach($show->Environment as $key => $env)
                        <div class="item{!! $key == 0 ? ' active' : '' !!}">
                            <img src="{!! url('/assets/environment/'. $env->upload_json->interna) !!}" alt="{!! $env->name_json->portugues !!}">
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        @include('site.elements.nav-bar')
        <div class="content">
            <div class="content-options" style="background-color: #30BFA9;">
                <a href="#">
                    <figure class="lupa" style="background-color: #30BFA9;">
                        <img src="{!! url('/assets/images/icons/lupa.png') !!}" alt="Lupa">
                    </figure>
                </a>
                <a href="#">
                    <figure class="modulos">
                        <img src="{!! url('/assets/images/icons/opcoes.png') !!}" alt="">
                    </figure>
                </a>
                <a href="#">
                    <figure class="modulos">
                        <img src="{!! url('/assets/images/icons/opcoes-2.png') !!}" alt="">
                    </figure>
                </a>
            </div>
            <div class="owl-carousel owl-theme" id="owl-carousel">
                @foreach($show->Environment as $env)
                    <a href="javascript:void(0)" class="item">
                        <figure>
                            <img src="{!! url('/assets/environment/'. $env->upload_json->home) !!}" alt="">
                            <figcaption>
                                {!! $env->name_json->portugues !!}
                            </figcaption>
                        </figure>
                    </a>
                @endforeach
            </div>
        </div>
    </main>
    <div class="clearfix"></div>

    @include('site.elements.footer-show')

    <div class="fd_overlay"></div>
    <div class="fd_modal">
        <a href="#" class="close-modal"><img src="{!! url('/assets/images/icons/close-modal.png') !!}" alt=""></a>
        <div id="carousel-modal" class="carousel slide" data-ride="carousel">
            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                @foreach($show->Environment as $key => $env)
                    <div class="item{!! $key == 0 ? ' active' : '' !!}">
                        <img src="{!! url('/assets/environment/'. $env->upload_json->home) !!}" alt=""  data-zoom-image="/assets/images/slider/slider-1_zoom.jpg">
                    </div>
                 @endforeach
            </div>

            <!-- Controls -->
            <a class="left carousel-control" href="#carousel-modal" role="button" data-slide="prev">
                <img src="{!! url('/assets/images/icons/arrow-left.png') !!}" alt="<<">
            </a>
            <a class="right carousel-control" href="#carousel-modal" role="button" data-slide="next">
                <img src="{!! url('/assets/images/icons/arrow-right.png') !!}" alt=">>">
            </a>
        </div>
    </div>


@endsection