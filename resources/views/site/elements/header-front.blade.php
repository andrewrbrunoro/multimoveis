<header>
    <figure class="pull-left">
        <a href="{!! route('front.index') !!}">
            <img src="{!! url('/assets/images/pdf-multimoveis-logo.png') !!}" alt="">
        </a>
    </figure>
    <nav class="flags pull-right">
        <ul class="list-inline">
            <li><a href="#"><img src="{!! url('/assets/images/icons/pt-br.png') !!}" alt=""></a></li>
            <li><a href="#"><img src="{!! url('/assets/images/icons/us-en.png') !!}" alt=""></a></li>
            <li><a href="#"><img src="{!! url('/assets/images/icons/es-es.png') !!}" alt=""></a></li>
            <li><a href="{!! route('line.index') !!}"><img src="{!! url('/assets/images/icons/lock.png') !!}" alt=""></a></li>
        </ul>
    </nav>
    <nav class="navbar navbar-static">
        <ul class="nav navbar-nav">
            <li><a href="{!! route('about.index') !!}">A EMPRESA</a></li>
            <li><a href="{!! route('video.index') !!}">VÍDEO INSTITUCIONAL</a></li>
            <li><a href="{!! route('contact.index') !!}">FALE CONOSCO</a></li>
            <li>
                <form class="" action="" method="post">
                    <input type="text" name="search" id="search" placeholder="BUSCA" required>
                </form>
            </li>
        </ul>
    </nav>
    <nav class="pull-right">
        <ul class="list-inline social">
            <li><a href="https://www.facebook.com/multimoveisbr"><img src="{!! url('/assets/images/icons/facebook.png') !!}" alt=""></a></li>
            <li><a href="#"><img src="{!! url('/assets/images/icons/instagram.png') !!}" alt=""></a></li>
            <li><a href="#"><img src="{!! url('/assets/images/icons/blog.png') !!}" alt=""></a></li>
        </ul>
    </nav>
    <div class="clearfix"></div>
</header>