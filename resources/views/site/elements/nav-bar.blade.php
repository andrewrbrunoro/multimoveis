<nav class="navbar navbar-default">
    <ul class="nav navbar-nav">
        <li><a href="{!! route('about.index') !!}">A EMPRESA</a></li>
        <li class="active">
            <a href="#">PRODUTOS</a>
            <ul class="sub-menu">
                <li><a href="#" class="active">AMBIENTES</a></li>
                @foreach($products as $product)
                    <li>
                        <a href="#">{!! json_to_name($product->name, 'portugues') !!}</a>
                    </li>
                @endforeach
            </ul>
        </li>
        <li><a href="#">NOVIDADES</a></li>
        <li><a href="#">CATÁLOGO VIRTUAL</a></li>
        <li><a href="#">CRIE SEU PROJETO</a></li>
        <li><a href="{!! route('contact.index') !!}">CONTATO</a></li>
    </ul>
</nav>