<footer class="baby" style="background-color: #15C3DC;">
    <div class="content">
        <address>
            MULTIMÓVEIS<br />
            Rua Carlos Dreher Neto, 1918 - Caixa Postal 639<br />
            Bento Gonçalves - RS - Brasil<br />
            CEP 95706-440<br />
            54 2102 4000 - multimoveis@multimoveis.com<br />
        </address>
        <a href="#">
            <figure class="pull-right">
                <img src="{!! url('/assets/images/icons/dica-para-o-lojista.png') !!}" alt="Dicas para o lojista">
            </figure>
        </a>
    </div>
</footer>