@extends('front-interna')

@section('content')
    <style>
        h3, label {
            color: #FFF;
        }
    </style>
    <main class="container">
        @include('site.elements.header-front')
        <section class="home">
            <h3>Fale conosco</h3>
            {!! Form::open(['route' => 'contact.store']) !!}
            <div class="form-group">
                <label for="name">
                    Nome <strong class="text-danger">*</strong>
                </label>
                {!! Form::text('name', null, ['class' => 'form-control', 'required', 'placeholder' => 'Nome *']) !!}
            </div>
            <div class="form-group">
                <label for="email">
                    E-mail <strong class="text-danger">*</strong>
                </label>
                {!! Form::text('email', null, ['class' => 'form-control', 'required', 'placeholder' => 'E-mail *']) !!}
            </div>
            <div class="form-group">
                <label for="phone">
                    Telefone <strong class="text-danger">*</strong>
                </label>
                {!! Form::text('phone', null, ['class' => 'form-control', 'required', 'placeholder' => 'Telefone *']) !!}
            </div>
            <div class="form-group">
                <label for="message">
                    Mensagem <strong class="text-danger">*</strong>
                </label>
                {!! Form::textarea('message', null, ['class' => 'form-control', 'required', 'placeholder' => 'Digite aqui a mensagem *']) !!}
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-success">
                    Enviar contato
                </button>
            </div>
            {!! Form::close() !!}
        </section>
    </main>
    <div class="clearfix"></div>

    @include('site.elements.footer-show')

@endsection