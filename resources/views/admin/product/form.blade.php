<div class="white-box" id="product">
    <!-- environments -->
    <div class="row" id="environment" languages='{!! setting('languages') !!}'>
        <input type="hidden" name="itemsRemove" v-model="itemsRemove"/>
        <ul class="nav nav-tabs list-inline">
            @foreach(setting('languages', true) as $key => $language)
                <li{!! $key == 0 ? ' class="active"' : '' !!}>
                    <a href="#name-{!! $language->slug !!}" data-toggle="tab">
                        <span class="hidden-xs">{!! $language->name !!}</span>
                    </a>
                </li>
            @endforeach
        </ul>
        <div class="tab-content">
            @foreach(setting('languages', true) as $key => $language)
                <div class="tab-pane fade{!! $key == 0 ? ' active in' : '' !!}" id="name-{!! $language->slug !!}">
                    <div class="form-group">
                        <label for="" class="col-md-12">
                            Nome/Título do Módulo em {!! $language->name !!} <strong class="text text-danger">*</strong>
                        </label>
                        <div class="col-md-12">
                            {!! Form::text('name['.$language->slug.']', null, ['class' => 'form-control', 'placeholder' => 'Nome no idioma '. $language->name]) !!}
                        </div>
                    </div>
                </div>
            @endforeach
        </div>

        <ul class="nav nav-tabs list-inline">
            @foreach(setting('languages', true) as $key => $language)
                <li{!! $key == 0 ? ' class="active"' : '' !!}>
                    <a href="#desc-{!! $language->slug !!}" data-toggle="tab">
                        <span class="hidden-xs">{!! $language->name !!}</span>
                    </a>
                </li>
            @endforeach
        </ul>
        <div class="tab-content">
            @foreach(setting('languages', true) as $key => $language)
                <div class="tab-pane fade{!! $key == 0 ? ' active in' : '' !!}" id="desc-{!! $language->slug !!}">
                    <div class="form-group">
                        <label for="" class="col-md-12">
                            Descrição do Módulo em {!! $language->name !!} <strong class="text text-danger">*</strong>
                        </label>
                        <div class="col-md-12">
                            {!! Form::textarea('descriptions['.$language->slug.']', null, ['class' => 'form-control', 'placeholder' => 'Descrição no idioma '. $language->name]) !!}
                        </div>
                    </div>
                </div>
            @endforeach
        </div>

        <div class="col-lg-6">
            <div class="form-group">
                <label class="col-md-12">
                    Ambiente <strong class="text-info">(Opcional)</strong>
                </label>
                <div class="col-md-12">
                    {!! Form::select('environment_id', \App\Environment::get()->pluck('reference', 'id'), null, ['id' => 'environment', 'class' => 'form-control', 'placeholder' => 'Selecione o ambiente']) !!}
                </div>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="form-group">
                <label class="col-md-12">
                    Altura <strong class="text-info">(Opcional)</strong>
                </label>
                <div class="col-md-12">
                    {!! Form::text('height', null, ['class' => 'form-control', 'v-model' => 'height', 'placeholder' => 'Digite a altura']) !!}
                </div>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="form-group">
                <label class="col-md-12">
                    Largura <strong class="text-info">(Opcional)</strong>
                </label>
                <div class="col-md-12">
                    {!! Form::text('width', null, ['class' => 'form-control', 'v-model' => 'width', 'placeholder' => 'Digite a largura']) !!}
                </div>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="form-group">
                <label class="col-md-12">
                    Profundidade <strong class="text-info">(Opcional)</strong>
                </label>
                <div class="col-md-12">
                    {!! Form::text('depth', null, ['class' => 'form-control', 'v-model' => 'depth', 'placeholder' => 'Digite a profundidade']) !!}
                </div>
            </div>
        </div>

        <div class="clearfix"></div>

        <h3 class="col-md-12">Variações</h3>

        <div class="col-lg-6">
            <div class="form-group">
                <label class="col-md-12">
                    Referência da variação <strong class="text-danger">*</strong>
                </label>
                <div class="col-md-12">
                    {!! Form::text('', null, ['class' => 'form-control', 'v-model' => 'reference', 'placeholder' => 'Digite a referência da variação']) !!}
                </div>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="form-group">
                <label class="col-md-12">
                    Cor <strong class="text-danger">*</strong>
                </label>
                <div class="col-md-12">
                    {!! Form::select('', \App\Color::get()->pluck('select', 'id'), null, ['v-model' => 'color_id', 'id' => 'color', 'class' => 'form-control', 'placeholder' => 'Selecione a cor da variação']) !!}
                </div>
            </div>
        </div>

        <div class="col-lg-12">
            <div class="form-group">
                <label class="col-md-12">
                    Imagem <strong class="text-danger">*</strong>
                </label>
                <div class="col-md-12">
                    <img v-if="upload.path" v-bind:src="upload.path + '/' + upload.w" width="50" />
                    {!! Form::file('', ['v-on:change' => 'tmp_image($event)', 'id' => 'upload-clear', 'class' => 'form-control', 'placeholder' => 'Imagem']) !!}
                    {!! Form::hidden('', null, ['v-model' => 'upload']) !!}
                </div>
            </div>
            <div class="form-group col-md-12">
                <button class="btn btn-success" v-on:click="save($event)" type="button">
                    Salvar Variação
                </button>
            </div>
        </div>
        <div class="col-md-12 table-responsive">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <td>Referência</td>
                    <td>Cor</td>
                    <td>Imagem</td>
                    <td>#</td>
                </tr>
                </thead>
                <tbody>
                    @if (isset($edit) && $edit->Grid->count())
                        @foreach($edit->Grid as $grid)
                            @php
                                $json = [
                                    'id' => $grid->id,
                                    'name' => $grid->name_json,
                                    'reference' => $grid->reference,
                                    'upload' => $grid->upload_json,
                                    'color_id' => $grid->color_id
                                ];
                            @endphp
                            <tr id="{!! $grid->id !!}">
                                <td>{!! $grid->reference !!}</td>
                                <td v-html="show_color('{!! $grid->color_id !!}')"></td>
                                <td>
                                    @if ($grid->upload)
                                        @if (file_exists(public_path('/assets/product/' . $grid->upload_json->w)))
                                            <img src="{!! url('/assets/product/' . $grid->upload_json->w) !!}" width="50" />
                                        @endif
                                    @endif
                                </td>
                                <td>
                                    <button class="btn btn-success btn-xs" v-on:click="edit($event, '{!! $grid->id !!}', true)" type="button">
                                        <i class="fa fa-pencil"></i>
                                    </button>
                                    <button class="btn btn-danger btn-xs" v-on:click="destroy($event, '{!! $grid->id !!}', true)" type="button">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                    <input type="hidden" v-bind:value='toString({!! json_encode($json) !!})' name="grids[]" />
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    <tr v-for="(k, o) in rows">
                        <td>@{{ o.reference }}</td>
                        <td>@{{ show_color(o.color_id) }}</td>
                        <td>@{{ o.height }}/@{{ o.width }}/@{{ o.depth }}</td>
                        <td>
                            <img v-bind:src="o.upload.path + '/' + o.upload.w" width="50" />
                        </td>
                        <td>
                            <button class="btn btn-success btn-xs" v-on:click="edit($event, o.id)" type="button">
                                <i class="fa fa-pencil"></i>
                            </button>
                            <button class="btn btn-danger btn-xs" v-on:click="destroy($event, o.id)" type="button">
                                <i class="fa fa-trash"></i>
                            </button>
                            <input type="hidden" v-bind:value="toString(o)" name="grids[]" />
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

<hr />
<div class="col-lg-12">
    <div class="form-group">
        <button class="btn btn-primary" type="submit">
            Salvar Módulo
        </button>
    </div>
</div>

@section('js')
    <script type="text/javascript">
        function guid() {
            function s4() {
                return Math.floor((1 + Math.random()) * 0x10000)
                        .toString(16)
                        .substring(1);
            }
            return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
                    s4() + '-' + s4() + s4() + s4();
        }
        function select (value, key, object, returnObject) {
            var $return = false;
            Object.keys(object).map(function(k, r) {
                if (object[k][key] == value) {
                    $return = returnObject ? JSON.parse(JSON.stringify(object[k])) : k;
                }
            });
            return $return;
        }
        new Vue({
            el: '#product',
            data: {
                itemsRemove: [],
                id: '',
                color_id: '',
                reference: '',
                upload: {},
                rows: []
            },
            methods: {
                validate: function() {
                    var $return = true;

                    if (this.color_id == '' || this.reference == '' || this.upload) {
                        toastr.error('Os campos com * são obrigatórios para cadastrar a variação.');
                        $return = false;
                    }

                    return $return;
                },
                save: function (e) {
                    e.preventDefault();
                    this.rows.push({
                        id: guid(),
                        color_id: this.color_id,
                        reference: this.reference,
                        upload: this.upload
                    });
                    this.clear_fields();
                },
                tmp_image: function(e) {
                    var self = this;
                    var form = new FormData();
                    form.append('file', e.target.files[0]);
                    form.append('path', 'uploads/tmp-image/');
                    $.ajax({
                        url: Laravel.url + '/api/tmp-image',
                        method: 'post',
                        cache: false,
                        data: form,
                        contentType: false,
                        processData: false,
                        success: function (r) {
                            if (r.error == true)
                                toastr.error(r.msg);
                            else {
                                self.upload = r.data;
                                toastr.success(r.msg);
                            }
                        }
                    });
                },
                clear_fields: function () {
                    this.id = '';
                    this.color_id = '';
                    this.name = {};
                    this.reference = '';
                    this.upload = {};
                    $('#upload-clear').val('');
                },
                toString: function (o) {
                    return JSON.stringify(o);
                },
                show_color: function(id) {
                    if (id == '')
                        return '';
                    return $('#color').find('[value="'+id+'"]').text();
                },
                show_environment: function(id) {
                    if (id == '')
                        return '';
                    return $('#environment').find('[value="'+id+'"]').text();
                },
                edit: function (e, id, edit) {
                    e.preventDefault();
                    var o;
                    if (edit) {
                        var object = JSON.parse($('[name^="grids"]').val());
                        o = select(id, 'id', [object], true);
                    } else {
                        o = this.rows[select(id, 'id', this.rows, false)];
                    }

                    this.id = o.id;
                    this.color_id = o.color_id;
                    this.name = o.name;
                    this.reference = o.reference;
                    this.upload = o.upload;

                    if (!edit)
                        this.rows.$remove(o);
                    else {
                        $('#' + id).remove();
                    }
                },
                destroy: function (e, id) {
                    e.preventDefault();

                    var o = this.rows[select(id, 'id', this.rows, false)];
                    this.itemsRemove.push(id);
                    this.rows.$remove(o);
                }
            }
        })
    </script>
@endsection