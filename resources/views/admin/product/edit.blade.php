@extends('admin')

@section('sn')
    Editar Módulo
@endsection

@section('b')
    <li>
        <a href="{!! route('product.index') !!}">
            Módulos
        </a>
    </li>
    <li class="active">
        Editar Módulo
    </li>
@endsection

@section('act')
    <div class="col-lg-2 col-sm-4 col-xs-12">
        <a href="{!! route('product.index') !!}" class="btn btn-block btn-warning">
            <i class="fa fa-arrow-left"></i> Voltar
        </a>
    </div>
@endsection

@section('content')
    {!! Form::model($edit, ['route' => ['product.update', $edit->id], 'method' => 'PATCH', 'files' => true, 'class' => 'form-material form-horizontal']) !!}
    @include('admin.product.form')
    {!! Form::close() !!}
@endsection