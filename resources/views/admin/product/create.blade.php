@extends('admin')

@section('sn')
    Novo Módulo
@endsection

@section('b')
    <li>
        <a href="{!! route('product.index') !!}">
            Módulos
        </a>
    </li>
    <li class="active">
        Novo Módulo
    </li>
@endsection

@section('act')
    <div class="col-lg-2 col-sm-4 col-xs-12">
        <a href="{!! route('product.index') !!}" class="btn btn-block btn-warning">
            <i class="fa fa-arrow-left"></i> Voltar
        </a>
    </div>
@endsection

@section('content')
    {!! Form::open(['route' => 'product.store', 'files' => true, 'class' => 'form-material form-horizontal']) !!}
    @include('admin.product.form')
    {!! Form::close() !!}
@endsection