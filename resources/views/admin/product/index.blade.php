@extends('admin')

@section('sn')
    Módulos
@endsection

@section('b')
    <li class="active">
        Módulos
    </li>
@endsection

@section('act')
    <div class="col-lg-3 col-sm-4 col-xs-12">
        <a href="{!! route('product.create') !!}" class="btn btn-block btn-brown" title="Clique aqui para adicionar um novo módulo">
            <i class="fa fa-plus"></i> Cadastrar Módulo
        </a>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="white-box">
                {!! Form::open(['route' => ['line.index', old('name')], 'method' => 'GET']) !!}
                <div class="row">
                    <div class="form-group{!! $errors->first('name') ? ' has-error' : '' !!}">
                        <div class="col-lg-10">
                            {!! Form::text('name', request()->has('name') ? request('name') : null, ['class' => 'form-control', 'placeholder' => 'Procurar por nome']) !!}
                            {!! $errors->first('name', '<span class="help-block">:message</span>') !!}
                        </div>
                        <div class="col-lg-2">
                            <button type="submit" class="btn btn-block btn-primary">
                                <i class="fa fa-search"></i> Procurar
                            </button>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    <div class="row el-element-overlay">
        @if (isset($rows) && $rows->count())
            <div class="col-lg-12">
                <div class="white-box">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <td>Nome</td>
                            <td>Ambiente</td>
                            <td>Qtd. Variações</td>
                            <td>Altura/Largura/Profundidade</td>
                            <td>#Opções</td>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($rows as $row)
                            <tr>
                                <td>{!! json_to_name($row->name, 'portugues') !!}</td>
                                <td>{!! $row->Environment ? json_to_name($row->Environment->name, 'portugues') : '' !!}</td>
                                <td>{!! $row->Grid->count() !!}</td>
                                <td>
                                    {!! $row->height !!}/{!! $row->width !!}/{!! $row->depth !!}
                                </td>
                                <td>
                                    <a href="{!! route('product.edit', $row->id) !!}" class="btn btn-success btn-xs pull-left">
                                        <i class="fa fa-pencil"></i>
                                    </a>
                                    {!! Form::open(['route' => ['product.destroy', $row->slug], 'method' => 'DELETE', 'class' => 'pull-left delete']) !!}
                                    <button class="btn btn-danger btn-xs" type="submit">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        @endif
    </div>
    @if (isset($rows))
        <div class="row">
            <div class="col-lg-12">
                @include('admin.elements.pagination', ['paginator' => $rows])
            </div>
        </div>
    @endif
@endsection

@section('js')
    {!! Html::script('/js/components/delete.js') !!}
@endsection