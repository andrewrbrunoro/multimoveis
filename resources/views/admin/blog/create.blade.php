@extends('admin')

@section('sn')
    Cadastrar Novidade
@endsection

@section('b')
    <li>
        <a href="{!! route('blog.index') !!}">
            Novidades
        </a>
    </li>
    <li class="active">
        Cadastrar Novidade
    </li>
@endsection

@section('act')
    <div class="col-lg-2 col-sm-4 col-xs-12">
        <a href="{!! route('blog.index') !!}" class="btn btn-block btn-warning">
            <i class="fa fa-arrow-left"></i> Voltar
        </a>
    </div>
@endsection

@section('content')
    {!! Form::open(['route' => 'blog.store', 'files' => true, 'class' => 'form-material form-horizontal']) !!}
    @include('admin.blog.form')
    {!! Form::close() !!}
@endsection