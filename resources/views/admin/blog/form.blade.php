<div class="white-box">

    <div class="form-group{!! $errors->first('status') ? ' has-error' : '' !!}">
        <label for="status" class="col-md-12">
            Status <strong class="text-danger">*</strong>
        </label>
        <div class="clearfix" style="padding-bottom: 11px"></div>
        <div class="col-md-12">
            <div class="radio radio-success radio-circle radio-inline">
                {!! Form::radio('status', '1', ['checked']) !!}
                <label for="status"> Ativo </label>
            </div>
            <div class="radio radio-danger radio-circle radio-inline">
                {!! Form::radio('status', '0') !!}
                <label for="status"> Inativo </label>
            </div>
        </div>
    </div>

    <div class="form-group{!! $errors->first('upload') ? ' has-error' : '' !!}">
        <label class="col-md-12" for="upload">
            Imagem <strong class="text-danger">*</strong>
        </label>
        <div class="col-md-12">
            @if (isset($edit) && $edit->filename != '' && isset($edit->upload['wh']))
                @if (file_exists(public_path('/assets/post/'.$edit->upload['wh'])))
                    <img src="{!! url('/assets/post/' . $edit->upload['wh']) !!}" alt=""/>
                @endif
            @endif
            {!! Form::file('upload', ['class' => 'form-control', 'placeholder' => 'Imagem']) !!}
            {!! $errors->first('upload', '<span class="help-block">:message</span>') !!}
        </div>
    </div>

    <div class="form-group{!! $errors->first('title') ? ' has-error' : '' !!}">
        <label class="col-md-12" for="title">
            Título <strong class="text-danger">*</strong>
        </label>
        <div class="col-md-12">
            {!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Título']) !!}
            {!! $errors->first('title', '<span class="help-block">:message</span>') !!}
        </div>
    </div>

    <div class="form-group{!! $errors->first('resume') ? ' has-error' : '' !!}">
        <label class="col-md-12" for="resume">
            Resumo (300 caracteres) <strong class="text-danger">*</strong>
        </label>
        <div class="col-md-12">
            {!! Form::textarea('resume', null, ['class' => 'form-control', 'placeholder' => 'Resumo de até 300 caracteres']) !!}
            {!! $errors->first('resume', '<span class="help-block">:message</span>') !!}
        </div>
    </div>

    <div class="form-group{!! $errors->first('content') ? ' has-error' : '' !!}">
        <label class="col-md-12" for="content">
            Conteúdo <strong class="text-danger">*</strong>
        </label>
        <div class="col-md-12">
            {!! Form::textarea('content', null, ['class' => 'form-control summernote', 'placeholder' => 'Conteúdo']) !!}
            {!! $errors->first('content', '<span class="help-block">:message</span>') !!}
        </div>
    </div>

</div>

<hr/>
<div class="col-lg-12">
    <div class="form-group">
        <button class="btn btn-primary" type="submit">
            Salvar Novidade
        </button>
    </div>
</div>

@section('css')
    {!! Html::style('/plugins/summernote/summernote.css') !!}
@endsection
@section('js')
    {!! Html::script('/plugins/summernote/summernote.min.js') !!}
    <script type="text/javascript">
        $(document).ready(function () {
            $('.summernote').summernote({height: 400});
        });
    </script>
@endsection