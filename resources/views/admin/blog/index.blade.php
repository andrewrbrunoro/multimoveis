@extends('admin')

@section('sn')
    Novidades
@endsection

@section('b')
    <li class="active">
        Novidades
    </li>
@endsection

@section('act')
    <div class="col-lg-3 col-sm-4 col-xs-12">
        <a href="{!! route('blog.create') !!}" class="btn btn-block btn-brown" title="Clique aqui para adicionar uma novidade">
            <i class="fa fa-plus"></i> Cadastrar Novidade
        </a>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="white-box">
                {!! Form::open(['route' => ['lead.index', old('name')], 'method' => 'GET']) !!}
                <div class="row">
                    <div class="form-group{!! $errors->first('name') ? ' has-error' : '' !!}">
                        <div class="col-lg-10">
                            {!! Form::text('name', request()->has('name') ? request('name') : null, ['class' => 'form-control', 'placeholder' => 'Procurar por nome']) !!}
                            {!! $errors->first('name', '<span class="help-block">:message</span>') !!}
                        </div>
                        <div class="col-lg-2">
                            <button type="submit" class="btn btn-block btn-primary">
                                <i class="fa fa-search"></i> Procurar
                            </button>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    <div class="row el-element-overlay">
        @if (isset($rows) && $rows->count())
            <div class="col-lg-12">
                <div class="white-box">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <td>Título</td>
                            <td>URl</td>
                            <td>Views</td>
                            <td>Status</td>
                            <td>#Opções</td>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($rows as $row)
                            <tr>
                                <td>{!! $row->title !!}</td>
                                <td>{!! url('/novidade/' . $row->slug) !!}</td>
                                <td>{!! $row->views !!}</td>
                                <td>
                                    {!! $row->status != 1 ? '<label class="label label-danger">Inativo</label>' : '<label class="label label-success">Ativo</label>' !!}
                                </td>
                                <td>
                                    <a href="{!! route('blog.edit', $row->slug) !!}" class="btn btn-success btn-xs pull-left">
                                        <i class="fa fa-pencil"></i>
                                    </a>
                                    {!! Form::open(['route' => ['blog.destroy', $row->slug], 'method' => 'DELETE', 'class' => 'pull-left']) !!}
                                    <button class="btn btn-danger btn-xs" type="submit">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        @endif
    </div>
    @if (isset($rows))
        <div class="row">
            <div class="col-lg-12">
                @include('admin.elements.pagination', ['paginator' => $rows])
            </div>
        </div>
    @endif
@endsection
