@extends('admin')

@section('sn')
    Novo Ambiente
@endsection

@section('b')
    <li>
        <a href="{!! route('product.index') !!}">
            Ambientes
        </a>
    </li>
    <li class="active">
        Novo Ambiente
    </li>
@endsection

@section('act')
    <div class="col-lg-2 col-sm-4 col-xs-12">
        <a href="{!! route('line.index') !!}" class="btn btn-block btn-warning">
            <i class="fa fa-arrow-left"></i> Voltar
        </a>
    </div>
@endsection

@section('content')
    @if (request()->has('line'))
        {!! Form::open(['route' => ['environment.store', 'line' => request('line')], 'files' => true, 'class' => 'form-material form-horizontal']) !!}
    @else
        {!! Form::open(['route' => 'environment.store', 'files' => true, 'class' => 'form-material form-horizontal']) !!}
    @endif
    @include('admin.environment.form')
    {!! Form::close() !!}
@endsection