<div class="white-box" id="line">
    @include('admin.line.environment')
</div>
<hr/>
<div class="col-lg-12">
    <div class="form-group">
        <button class="btn btn-primary" type="submit">
            Salvar Linha
        </button>
    </div>
</div>

@section('js')
    {!! Html::script('/js/components/line-module-product.js') !!}
@endsection