@extends('admin')

@section('sn')
    Ambientes - {!! $line->name !!}
@endsection

@section('b')
    <li class="active">
        Ambientes - {!! $line->name !!}
    </li>
@endsection

@section('act')
    <div class="col-lg-2 col-sm-4 col-xs-12">
        <a href="{!! route('line.index') !!}" class="btn btn-block btn-warning">
            <i class="fa fa-arrow-left"></i> Voltar
        </a>
    </div>
    <div class="col-lg-3 col-sm-4 col-xs-12">
        <a href="{!! route('environment.create', ['line' => $line->id]) !!}" class="btn btn-block btn-brown" title="Clique aqui para adicionar um novo ambiente">
            <i class="fa fa-plus"></i> Cadastrar Ambiente
        </a>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="white-box">
                {!! Form::open(['route' => ['line.index', old('name')], 'method' => 'GET']) !!}
                <div class="row">
                    <div class="form-group{!! $errors->first('name') ? ' has-error' : '' !!}">
                        <div class="col-lg-10">
                            {!! Form::text('name', request()->has('name') ? request('name') : null, ['class' => 'form-control', 'placeholder' => 'Procurar por nome']) !!}
                            {!! $errors->first('name', '<span class="help-block">:message</span>') !!}
                        </div>
                        <div class="col-lg-2">
                            <button type="submit" class="btn btn-block btn-primary">
                                <i class="fa fa-search"></i> Procurar
                            </button>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    <div class="row el-element-overlay">
        @if (isset($rows) && $rows->count())
            <div class="col-lg-12">
                <div class="white-box">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <td>Cor</td>
                            <td>Nome</td>
                            <td>Referencia</td>
                            <td>#Opções</td>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($rows as $row)
                            <tr>
                                @php
                                    $color = json_decode($row->Color->name)
                                @endphp
                                <td>{!! $color[0]->name !!}</td>
                                <td>{!! json_to_name($row->name, 'portugues') !!}</td>
                                <td>{!! $row->reference !!}</td>
                                <td>
                                    @if($row->upload)
                                        <img src="{!! url('/assets/environment/'.$row->upload_json->w) !!}" alt="" width="50" />
                                    @endif
                                </td>
                                <td>
                                    {!! Form::open(['route' => ['environment.destroy', $row->id], 'method' => 'DELETE', 'class' => 'pull-left delete']) !!}
                                    <button class="btn btn-danger btn-xs" type="submit">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        @endif
    </div>
@endsection

@section('js')
    {!! Html::script('/js/components/delete.js') !!}
@endsection