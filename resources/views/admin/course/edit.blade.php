@extends('admin')

@section('sn')
    Editar Curso {!! $edit->name !!}
@endsection

@section('b')
    <li>
        <a href="{!! route('course.index') !!}">
            Cursos
        </a>
    </li>
    <li class="active">
        Editar Curso {!! $edit->name !!}
    </li>
@endsection

@section('act')
    <div class="col-lg-2 col-sm-4 col-xs-12">
        <a href="{!! route('course.index') !!}" class="btn btn-block btn-warning">
            <i class="fa fa-arrow-left"></i> Voltar
        </a>
    </div>
    <div class="col-lg-2 col-sm-4 col-xs-12">
        <a href="{!! route('course.create') !!}" class="btn btn-block btn-success" title="Clique aqui para adicionar um novo curso">
            <i class="fa fa-plus"></i> Adicionar Curso
        </a>
    </div>
@endsection

@section('content')
    {!! Form::model($edit,['route' => ['course.update', $edit->slug], 'method' => 'PATCH', 'class' => 'form-material form-horizontal']) !!}
    @include('admin.course.form')
    {!! Form::close() !!}
@endsection