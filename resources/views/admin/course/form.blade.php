<div class="row">
    <div class="col-sm-12">
        <div class="white-box">
            <div class="row">
                <div class="col-lg-4">
                    <div class="form-group{!! $errors->first('status') ? ' has-error' : '' !!}">
                        <label for="status">
                            Status <strong class="text-danger">*</strong>
                        </label>
                        <div class="clearfix" style="padding-bottom: 11px"></div>
                        <div class="radio radio-success radio-circle radio-inline">
                            {!! Form::radio('status', true, ['checked']) !!}
                            <label for="status"> Ativo </label>
                        </div>
                        <div class="radio radio-danger radio-circle radio-inline">
                            {!! Form::radio('status', false) !!}
                            <label for="status"> Inativo </label>
                        </div>
                    </div>
                    <div class="form-group{!! $errors->first('discipline_id') ? ' has-error' : '' !!}">
                        <label class="col-md-12" for="email">
                            Disciplina <strong class="text-danger">*</strong>
                        </label>
                        <div class="col-md-12">
                            {!! Form::select('discipline_id', \App\Discipline::pluck('name', 'id'), null, ['class' => 'form-control', 'placeholder' => 'Selecione a disciplina']) !!}
                            {!! $errors->first('discipline_id', '<span class="help-block">:message</span>') !!}
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group{!! $errors->first('name') ? ' has-error' : '' !!}">
                        <label class="col-md-12" for="name">
                            Nome <strong class="text-danger">*</strong>
                        </label>
                        <div class="col-md-12">
                            {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Exemplo: Curso de Matemática, Português avançado e etc...']) !!}
                            {!! $errors->first('name', '<span class="help-block">:message</span>') !!}
                        </div>
                    </div>
                    <div class="form-group{!! $errors->first('slug') ? ' has-error' : '' !!}">
                        <label class="col-md-12" for="last_name">
                            SLUG <strong class="text-info">(Preencha o título)</strong>
                        </label>
                        <div class="col-md-12">
                            {!! Form::text('slug', null, ['class' => 'form-control', 'placeholder' => 'Exemplo: matematica-basica, portugues-avancado']) !!}
                            {!! $errors->first('slug', '<span class="help-block">:message</span>') !!}
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group{!! $errors->first('release_date') ? ' has-error' : '' !!}">
                        <label class="col-md-12" for="phone">
                            Data de lançamento <strong class="text-info">(Opcional)</strong>
                        </label>
                        <div class="col-md-12">
                            {!! Form::text('release_date', null, ['class' => 'form-control', 'placeholder' => 'Exemplo: '.date('d/m/Y H:i')]) !!}
                            {!! $errors->first('release_date', '<span class="help-block">:message</span>') !!}
                        </div>
                    </div>
                    <div class="form-group{!! $errors->first('close_date') ? ' has-error' : '' !!}">
                        <label class="col-md-12" for="password_confirm">
                            Data de encerramento <strong class="text-info">(Opcional)</strong>
                        </label>
                        <div class="col-md-12">
                            {!! Form::password('close_date', ['class' => 'form-control', 'placeholder' => 'Exemplo: '.date('d/m/Y H:i')]) !!}
                            {!! $errors->first('close_date', '<span class="help-block">:message</span>') !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="white-box" id="course-attributes" count="{!! isset($edit) && $edit->Content ? $edit->Content->count() : 0 !!}">
            <input type="hidden" name="remove_tabs" v-model="itemsRemove" />
            <div class="sttabs tabs-style-linetriangle">
                <nav>
                    <ul>
                        @if (isset($edit) && $edit->Content)
                            @foreach($edit->Content as $key => $tab)
                                <li v-bind:class="current == {!! $key !!} ? 'tab-current' : ''" v-on:click="current = {!! $key !!}" id="tab-{!! $tab->key !!}">
                                    <a href="javascript:void(0);">
                                        <span>
                                            {!! $tab->title !!}
                                            <button type="button" class="btn btn-danger btn-xs" v-on:click="destroy('{!! $tab->key !!}', $event)">
                                                <i class="fa fa-times"></i>
                                            </button>
                                        </span>
                                    </a>
                                </li>
                            @endforeach
                        @elseif(session()->has('course_attributes'))
                            @php
                                $i = 0;
                            @endphp
                            @foreach(session('course_attributes') as $key => $tab)
                                <li v-bind:class="current == {!! $i !!} ? 'tab-current' : ''" v-on:click="current = {!! $i !!}" id="tab-{!! str_slug($tab['title']) !!}">
                                    <a href="javascript:void(0);">
                                    <span>
                                        {!! $tab['title'] !!}
                                        <button type="button" class="btn btn-danger btn-xs" v-on:click="destroy('{!! str_slug($tab['title']) !!}', $event)">
                                            <i class="fa fa-times"></i>
                                        </button>
                                    </span>
                                    </a>
                                </li>
                                @php
                                    $i++;
                                @endphp
                            @endforeach
                        @else
                            @foreach(setting('course-tabs', true) as $key => $tab)
                            <li v-bind:class="current == {!! $key !!} ? 'tab-current' : ''" v-on:click="current = {!! $key !!}" id="tab-{!! $tab->key !!}">
                                <a href="javascript:void(0);">
                                    <span>
                                        {!! $tab->title !!}
                                        <button type="button" class="btn btn-danger btn-xs" v-on:click="destroy('{!! $tab->key !!}', $event)">
                                            <i class="fa fa-times"></i>
                                        </button>
                                    </span>
                                </a>
                            </li>
                            @endforeach
                        @endif
                        <li v-bind:class="current == tab_index ? 'tab-current' : ''" v-on:click="current = tab_index" v-for="(key, tab) in tabs" v-bind:id="'tab-'+tab.key">
                            <a href="javascript:void(0);">
                                <span>
                                    @{{ tab.title }}
                                    <button type="button" class="btn btn-danger btn-xs" v-on:click="destroy(tab.key, $event)">
                                        <i class="fa fa-times"></i>
                                    </button>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span data-toggle="modal" data-target="#add_tab"><i class="fa fa-plus"></i> Adicionar TAB</span>
                            </a>
                        </li>
                    </ul>
                </nav>
                <div class="content-wrap">
                    @if (isset($edit) && $edit->Content)
                        @foreach($edit->Content as $key => $tab)
                            <section v-show="current == {!! $key !!}" class="content-current" id="course-attributes-{!! $tab->key !!}">
                                <div class="row">
                                    <div class="form-group">
                                        <label for="">
                                            Conteúdo - <strong>{!! $tab->title !!}</strong> <strong class="text-info">(Opcional)</strong>
                                        </label>
                                        {!! Form::textarea('course_attributes['.$tab->key.'][value]', $tab->value, ['class' => 'textarea_editor form-control', 'rows' => 4, 'placeholder' => 'Digite aqui o conteúdo']) !!}
                                        {!! Form::hidden('course_attributes['.$tab->key.'][title]', $tab->title) !!}
                                    </div>
                                </div>
                            </section>
                        @endforeach
                    @elseif(session()->has('course_attributes'))
                        @php
                            $i = 0;
                        @endphp
                        @foreach(session('course_attributes') as $key => $tab)
                            <section v-show="current == {!! $i !!}" class="content-current" id="course-attributes-{!! str_slug($tab['title']) !!}">
                                <div class="row">
                                    <div class="form-group">
                                        <label for="">
                                            Conteúdo - <strong>{!! $tab['title'] !!}</strong> <strong class="text-info">(Opcional)</strong>
                                        </label>
                                        {!! Form::textarea('course_attributes['.str_slug($tab['title']).'][value]', null, ['class' => 'textarea_editor form-control', 'rows' => 4, 'placeholder' => 'Digite aqui o conteúdo']) !!}
                                        {!! Form::hidden('course_attributes['.str_slug($tab['title']).'][title]', $tab['title']) !!}
                                    </div>
                                </div>
                            </section>
                            @php
                                $i++;
                            @endphp
                        @endforeach
                    @else
                        @foreach(setting('course-tabs', true) as $key => $tab)
                            <section v-show="current == {!! $key !!}" class="content-current" id="course-attributes-{!! $tab->key !!}">
                                <div class="row">
                                    <div class="form-group">
                                        <label for="">
                                            Conteúdo - <strong>{!! $tab->title !!}</strong> <strong class="text-info">(Opcional)</strong>
                                        </label>
                                        {!! Form::textarea('course_attributes['.$tab->key.'][value]', null, ['class' => 'textarea_editor form-control', 'rows' => 4, 'placeholder' => 'Digite aqui o conteúdo']) !!}
                                        {!! Form::hidden('course_attributes['.$tab->key.'][title]', $tab->title) !!}
                                    </div>
                                </div>
                            </section>
                        @endforeach
                    @endif
                    <section v-show="current == tab_index" v-for="(key, tab) in tabs" class="content-current">
                        <div class="row">
                            <div class="form-group">
                                <label for="">
                                    Conteúdo - <strong>@{{ tab.title }}</strong> <strong class="text-info">(Opcional)</strong>
                                </label>
                                <textarea class="textarea_editor form-control" rows="4"
                                          v-bind:name="'course_attributes['+tab.key+'][value]'"
                                          placeholder="Digite aqui o conteúdo"></textarea>
                                <input type="hidden" v-bind:name="'course_attributes['+tab.key+'][title]'" v-bind:value="tab.title" />
                            </div>
                        </div>
                    </section>
                </div>
            </div>


            <div class="modal fade" id="add_tab" tabindex="-1" role="dialog" aria-labelledby="add_tab"
                 style="display: none;">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close" v-on:click="close_modal">
                                <span aria-hidden="true">×</span>
                            </button>
                            <h4 class="modal-title" id="exampleModalLabel1">Nova TAB</h4></div>
                        <div class="modal-body">
                            <div class="form-group">
                                <input type="text" class="form-control" v-model="new_tab" placeholder="Digite aqui o título da nova TAB"/>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal" v-on:click="close_modal">
                                Fechar
                            </button>
                            <button type="button" class="btn btn-primary" v-on:click="save_tab">Salvar TAB</button>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <hr />

        <div class="col-lg-12">
            <div class="form-group">
                <button class="btn btn-primary" type="submit">
                    Salvar Curso
                </button>
            </div>
        </div>
    </div>
</div>

@section('css')
    {!! Html::style('/plugins/summernote/summernote.css') !!}
@endsection

@section('js')
    {!! Html::script('/plugins/summernote/summernote.min.js') !!}
    {!! Html::script('/js/components/course-tabs.js') !!}
@endsection