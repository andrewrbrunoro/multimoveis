@extends('admin')

@section('sn')
    Cursos
@endsection

@section('b')
    <li class="active">
        Cursos
    </li>
@endsection

@section('act')
    <div class="col-lg-2 col-sm-4 col-xs-12">
        <a href="{!! route('course.create') !!}" class="btn btn-block btn-success" title="Clique aqui para adicionar um novo curso">
            <i class="fa fa-plus"></i> Adicionar Curso
        </a>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="white-box">
                {!! Form::open(['route' => ['course.index', old('name'), old('email')], 'method' => 'GET']) !!}
                <div class="row">
                    <div class="form-group{!! $errors->first('name') ? ' has-error' : '' !!}">
                        <div class="col-lg-5">
                            {!! Form::text('name', request()->has('name') ? request('name') : null, ['class' => 'form-control', 'placeholder' => 'Procurar por nome']) !!}
                            {!! $errors->first('name', '<span class="help-block">:message</span>') !!}
                        </div>
                        <div class="col-lg-5">
                            {!! Form::text('email', request()->has('email') ? request('email') : null, ['class' => 'form-control', 'placeholder' => 'Procurar por e-mail']) !!}
                            {!! $errors->first('email', '<span class="help-block">:message</span>') !!}
                        </div>
                        <div class="col-lg-2">
                            <button type="submit" class="btn btn-block btn-primary">
                                <i class="fa fa-search"></i> Procurar
                            </button>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    <div class="row el-element-overlay">
        @if (isset($rows) && $rows->count())
            <div class="col-lg-12">
                <div class="white-box">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <td>Curso</td>
                            <td>Disciplina</td>
                            <td>Qtd. Turmas</td>
                            <td>Data de lançamento</td>
                            <td>Data de encerramento</td>
                            <td>#</td>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($rows as $row)
                                <tr>
                                    <td>{!! $row->name !!}</td>
                                    <td>{!! $row->Discipline->name !!}</td>
                                    <td>0</td>
                                    <td>{!! sqlToDate($row->release_date) !!}</td>
                                    <td>{!! sqlToDate($row->close_date) !!}</td>
                                    <td>
                                        <a href="{!! route('course.edit', $row->slug) !!}" class="btn btn-success btn-xs pull-left">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                        {!! Form::open(['route' => ['course.destroy', $row->slug], 'method' => 'DELETE', 'class' => 'pull-left']) !!}
                                        <button class="btn btn-danger btn-xs" type="submit">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        @endif
    </div>
    @if (isset($rows))
        <div class="row">
            <div class="col-lg-12">
                @include('admin.elements.pagination', ['paginator' => $rows])
            </div>
        </div>
    @endif
@endsection