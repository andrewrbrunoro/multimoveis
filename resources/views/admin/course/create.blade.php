@extends('admin')

@section('sn')
    Novo Curso
@endsection

@section('b')
    <li>
        <a href="{!! route('course.index') !!}">
            Cursos
        </a>
    </li>
    <li class="active">
        Novo Curso
    </li>
@endsection

@section('act')
    <div class="col-lg-2 col-sm-4 col-xs-12">
        <a href="{!! route('course.index') !!}" class="btn btn-block btn-warning">
            <i class="fa fa-arrow-left"></i> Voltar
        </a>
    </div>
@endsection

@section('content')
    {!! Form::open(['route' => 'course.store', 'class' => 'form-material form-horizontal']) !!}
    @include('admin.course.form')
    {!! Form::close() !!}
@endsection