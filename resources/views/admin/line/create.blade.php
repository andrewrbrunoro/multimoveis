@extends('admin')

@section('sn')
    Nova Linha
@endsection

@section('b')
    <li>
        <a href="{!! route('line.index') !!}">
            Linhas
        </a>
    </li>
    <li class="active">
        Nova Linha
    </li>
@endsection

@section('act')
    <div class="col-lg-2 col-sm-4 col-xs-12">
        <a href="{!! route('line.index') !!}" class="btn btn-block btn-warning">
            <i class="fa fa-arrow-left"></i> Voltar
        </a>
    </div>
@endsection

@section('content')
    {!! Form::open(['route' => 'line.store', 'files' => true, 'class' => 'form-material form-horizontal']) !!}
    @include('admin.line.form')
    {!! Form::close() !!}
@endsection