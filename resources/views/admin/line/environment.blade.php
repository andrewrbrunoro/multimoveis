<!-- environments -->
<div class="row" id="environment">
    <input type="hidden" name="env_itemsRemove" v-model="env.itemsRemove" />
    <ul class="nav nav-tabs list-inline">
        @foreach(setting('languages', true) as $key => $language)
            <li{!! $key == 0 ? ' class="active"' : '' !!}>
                <a href="#env-{!! $language->slug !!}" data-toggle="tab">
                    <span class="hidden-xs">{!! $language->name !!}</span>
                </a>
            </li>
        @endforeach
    </ul>
    <div class="tab-content">
        @foreach(setting('languages', true) as $key => $language)
            <div class="tab-pane fade{!! $key == 0 ? ' active in' : '' !!}" id="env-{!! $language->slug !!}">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="" class="col-md-12">
                            Título do Ambiente em {!! $language->name !!} <strong class="text text-danger">*</strong>
                        </label>
                        <div class="col-md-12">
                            {!! Form::text('', null, ['v-model' => 'env.name.'.$language->slug, 'class' => 'form-control', 'placeholder' => 'Nome no idioma '. $language->name]) !!}
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
    <div class="col-lg-6">
        <div class="form-group">
            <label class="col-md-12">
                Referência do Ambiente <strong class="text-danger">*</strong>
            </label>
            <div class="col-md-12">
                {!! Form::text('', null, ['v-model' => 'env.reference', 'class' => 'form-control', 'placeholder' => 'Digite aqui a referência']) !!}
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="form-group}">
            <label class="col-md-12">
                Cor do Ambiente <strong class="text-danger">*</strong>
            </label>
            <div class="col-md-12">
                {!! Form::select('', \App\Color::get()->pluck('select', 'id'), null, ['v-model' => 'env.color_id', 'id' => 'color', 'class' => 'form-control', 'placeholder' => 'Selecione a cor do ambiente']) !!}
            </div>
        </div>
    </div>
    <div class="col-lg-12">
        <div class="form-group">
            <label class="col-md-12">
                Imagem do Ambiente <strong class="text-danger">*</strong>
            </label>
            <div class="col-md-12">
                <img v-if="env.upload.path" v-bind:src="env.upload.path + '/' + env.upload.w" width="50" />
                {!! Form::file('', ['v-on:change' => 'e_tmp_image($event)', 'id' => 'upload-clear', 'class' => 'form-control', 'placeholder' => 'Imagem']) !!}
                {!! Form::hidden('', null, ['v-model' => 'env.upload']) !!}
            </div>
        </div>
        <div class="form-group col-md-12">
            <button class="btn btn-success" v-on:click="e_save($event)" type="button">
                <i class="fa fa-plus"></i> @{{ env.wait == false ? 'Adicionar Ambiente' : 'Aguarde...' }}
            </button>
        </div>
    </div>
    <div class="col-md-12 table-responsive">
        <table class="table table-bordered">
            <thead>
            <tr>
                <td>Nome</td>
                <td>Cor</td>
                <td>Referência</td>
                <td>Imagem</td>
                <td>#</td>
            </tr>
            </thead>
            <tbody>
            @if (session()->has('environments'))
                @foreach(session()->get('environments') as $json)
                    @php
                        $env = json_decode($json);
                    @endphp
                    <tr id="{!! $env->id !!}">
                        <td>
                            @foreach($env->name as $name)
                                {!! $name !!}
                            @endforeach
                        </td>
                        <td v-html="e_show_color('{!! $env->color_id !!}')"></td>
                        <td>{!! $env->reference !!}</td>
                        <td>
                            @if ($env->upload)
                                <img src="{!! $env->upload->path . '/' . $env->upload->w !!}" width="50" />
                            @endif
                        </td>
                        <td>
                            {{--<button class="btn btn-success btn-xs" v-on:click="e_edit($event, '{!! $env->id !!}', true)" type="button">--}}
                                {{--<i class="fa fa-pencil"></i>--}}
                            {{--</button>--}}
                            <button class="btn btn-danger btn-xs" v-on:click="e_destroy($event, '{!! $env->id !!}', true)" type="button">
                                <i class="fa fa-trash"></i>
                            </button>
                            <input type="hidden" v-bind:value='e_toString({!! $json !!})' name="environments[]" />
                        </td>
                    </tr>
                @endforeach
            @endif
            @if (isset($edit) && $edit->Environment)
                @foreach($edit->Environment as $env)
                    @php
                        $json = [
                        'id' => $env->id,
                        'name' => $env->name_json,
                        'reference' => $env->reference,
                        'upload' => $env->upload_json,
                        'color_id' => $env->color_id
                        ];
                    @endphp
                    <tr id="{!! $env->id !!}">
                        <td>
                            @foreach($env->name_json as $name)
                                {!! $name !!}
                            @endforeach
                        </td>
                        <td v-html="e_show_color('{!! $env->color_id !!}')"></td>
                        <td>{!! $env->reference !!}</td>
                        <td>
                            @if ($env->upload)
                                @if (file_exists(public_path('/assets/environment/' . $env->upload_json->w)))
                                    <img src="{!! url('/assets/environment/' . $env->upload_json->w) !!}" width="50" />
                                @endif
                            @endif
                        </td>
                        <td>
                            {{--<button class="btn btn-success btn-xs" v-on:click="e_edit($event, '{!! $env->id !!}', true)" type="button">--}}
                                {{--<i class="fa fa-pencil"></i>--}}
                            {{--</button>--}}
                            <button class="btn btn-danger btn-xs" v-on:click="e_destroy($event, '{!! $env->id !!}', true)" type="button">
                                <i class="fa fa-trash"></i>
                            </button>
                            <input type="hidden" v-bind:value='e_toString({!! json_encode($json) !!})' name="environments[]" />
                        </td>
                    </tr>
                @endforeach
            @endif
            <tr v-for="(k, o) in env.rows" v-bind:id="o.id">
                <td>@{{ e_show_name(o.name) }}</td>
                <td>@{{ e_show_color(o.color_id) }}</td>
                <td>@{{ o.reference }}</td>
                <td>
                    <img v-bind:src="o.upload.path + '/' + o.upload.w" width="50" />
                </td>
                <td>
                    {{--<button class="btn btn-success btn-xs" v-on:click="e_edit($event, o.id)" type="button">--}}
                        {{--<i class="fa fa-pencil"></i>--}}
                    {{--</button>--}}
                    <button class="btn btn-danger btn-xs" v-on:click="e_destroy($event, o.id)" type="button">
                        <i class="fa fa-trash"></i>
                    </button>
                    <input type="hidden" v-bind:value="e_toString(o)" name="environments[]" />
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>