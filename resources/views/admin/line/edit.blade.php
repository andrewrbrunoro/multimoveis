@extends('admin')

@section('sn')
    Editar Linha
@endsection

@section('b')
    <li>
        <a href="{!! route('line.index') !!}">
            Linhas
        </a>
    </li>
    <li class="active">
        Nova Linha
    </li>
@endsection

@section('act')
    <div class="col-lg-2 col-sm-4 col-xs-12">
        <a href="{!! route('line.index') !!}" class="btn btn-block btn-warning">
            <i class="fa fa-arrow-left"></i> Voltar
        </a>
    </div>
    <div class="col-lg-2 col-sm-4 col-xs-12">
        <a href="{!! route('line.create') !!}" class="btn btn-block btn-success"
           title="Clique aqui para adicionar uma nova linha">
            <i class="fa fa-plus"></i> Adicionar Linha
        </a>
    </div>
@endsection

@section('content')
    {!! Form::model($edit, ['route' => ['line.update', $edit->slug], 'files' => true, 'method' => 'PATCH', 'class' => 'form-material form-horizontal']) !!}
    @include('admin.line.form')
    {!! Form::close() !!}
@endsection