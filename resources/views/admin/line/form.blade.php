<div class="white-box" id="line">

    <div class="row">
        <h3 class="col-sm-12 bg-info text-white">Linha</h3>
    </div>

    <div class="row">
        <div class="col-lg-6">
            <div class="form-group{!! $errors->first('name') ? ' has-error' : '' !!}">
                <label for="" class="col-md-12">
                    Nome/Título da Linha <strong class="text-danger">*</strong>
                </label>
                <div class="col-md-12">
                    {!! Form::text('name', null, ['required', 'class' => 'form-control', 'placeholder' => 'Nome/Título da linha']) !!}
                    {!! $errors->first('name', '<span class="help-block">:message</span>') !!}
                </div>
            </div>
        </div>
    </div>
    <ul class="nav nav-tabs list-inline">
        @foreach(setting('languages', true) as $key => $language)
            <li{!! $key == 0 ? ' class="active"' : '' !!}>
                <a href="#{!! $language->slug !!}" data-toggle="tab">
                    <span class="hidden-xs">{!! $language->name !!}</span>
                </a>
            </li>
        @endforeach
    </ul>
    <div class="tab-content">
        @foreach(setting('languages', true) as $key => $language)
            <div class="tab-pane fade{!! $key == 0 ? ' active in' : '' !!}" id="{!! $language->slug !!}">
                {!! Form::textarea('descriptions['.$language->slug.']', null, ['class' => 'form-control', 'placeholder' => 'Descrição em '. $language->name]) !!}
            </div>
        @endforeach
    </div>

    <hr />

    <div class="row">
        <h3 class="col-sm-12 bg-info text-white">Ambientes</h3>
    </div>

    @include('admin.line.environment')

    <div class="row">
        <h3 class="col-sm-12 bg-info text-white">Módulos</h3>
    </div>

    @include('admin.line.modules')

</div>
<hr/>
<div class="col-lg-12">
    <div class="form-group">
        <button class="btn btn-primary" type="submit">
            <i class="fa fa-check"></i> Salvar
        </button>
    </div>
</div>
@section('js')
    {!! Html::script('/js/components/line-module-product.js') !!}
@endsection