<div class="row" id="environment">
    <input type="hidden" name="module_itemsRemove" v-model="module.itemsRemove"/>
    <ul class="nav nav-tabs list-inline">
        @foreach(setting('languages', true) as $key => $language)
            <li{!! $key == 0 ? ' class="active"' : '' !!}>
                <a href="#name-{!! $language->slug !!}" data-toggle="tab">
                    <span class="hidden-xs">{!! $language->name !!}</span>
                </a>
            </li>
        @endforeach
    </ul>
    <div class="tab-content">
        @foreach(setting('languages', true) as $key => $language)
            <div class="tab-pane fade{!! $key == 0 ? ' active in' : '' !!}" id="name-{!! $language->slug !!}">
                <div class="form-group">
                    <label for="" class="col-md-12">
                        Nome/Título do Módulo em {!! $language->name !!} <strong class="text text-danger">*</strong>
                    </label>
                    <div class="col-md-12">
                        {!! Form::text('', null, ['v-model' => 'module.name.'.$language->slug, 'class' => 'form-control', 'placeholder' => 'Nome no idioma '. $language->name]) !!}
                    </div>
                </div>
            </div>
        @endforeach
    </div>

    <ul class="nav nav-tabs list-inline">
        @foreach(setting('languages', true) as $key => $language)
            <li{!! $key == 0 ? ' class="active"' : '' !!}>
                <a href="#desc-{!! $language->slug !!}" data-toggle="tab">
                    <span class="hidden-xs">{!! $language->name !!}</span>
                </a>
            </li>
        @endforeach
    </ul>
    <div class="tab-content">
        @foreach(setting('languages', true) as $key => $language)
            <div class="tab-pane fade{!! $key == 0 ? ' active in' : '' !!}" id="desc-{!! $language->slug !!}">
                <div class="form-group">
                    <label for="" class="col-md-12">
                        Descrição do Módulo em {!! $language->name !!} <strong class="text text-danger">*</strong>
                    </label>
                    <div class="col-md-12">
                        {!! Form::textarea('', null, ['v-model' => 'module.descriptions.'.$language->slug, 'class' => 'form-control', 'placeholder' => 'Descrição no idioma '. $language->name]) !!}
                    </div>
                </div>
            </div>
        @endforeach
    </div>

    <div class="col-lg-6">
        <div class="form-group">
            <label class="col-md-12">
                Ambiente do módulo <strong class="text-danger">*</strong>
            </label>
            <div class="col-md-12">
                <select id="environment" class="form-control" v-model="module.environment">
                    <option value="">Selecione um ambiente</option>
                    <option v-for="(k, o) in env.rows" v-bind:value="o.reference">@{{ o.reference }}</option>
                    @foreach(\App\Environment::get()->pluck('reference', 'id') as $index => $option)
                        <option value="{!! $index !!}">{!! $option !!}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>

    <div class="col-lg-6">
        <div class="form-group">
            <label class="col-md-12">
                Altura do módulo <strong class="text-info">(Opcional)</strong>
            </label>
            <div class="col-md-12">
                {!! Form::text('', null, ['class' => 'form-control', 'v-model' => 'module.height', 'placeholder' => 'Digite a altura']) !!}
            </div>
        </div>
    </div>

    <div class="col-lg-6">
        <div class="form-group">
            <label class="col-md-12">
                Largura do módulo <strong class="text-info">(Opcional)</strong>
            </label>
            <div class="col-md-12">
                {!! Form::text('', null, ['class' => 'form-control', 'v-model' => 'module.width', 'placeholder' => 'Digite a largura']) !!}
            </div>
        </div>
    </div>

    <div class="col-lg-6">
        <div class="form-group">
            <label class="col-md-12">
                Profundidade do módulo <strong class="text-info">(Opcional)</strong>
            </label>
            <div class="col-md-12">
                {!! Form::text('', null, ['class' => 'form-control', 'v-model' => 'module.depth', 'placeholder' => 'Digite a profundidade']) !!}
            </div>
        </div>
    </div>

    <div class="clearfix"></div>

    <h3 class="col-md-12">Variações</h3>

    <div class="col-lg-6">
        <div class="form-group">
            <label class="col-md-12">
                Referência da variação <strong class="text-danger">*</strong>
            </label>
            <div class="col-md-12">
                {!! Form::text('', null, ['class' => 'form-control', 'v-model' => 'module.reference', 'placeholder' => 'Digite a referência da variação']) !!}
            </div>
        </div>
    </div>

    <div class="col-lg-6">
        <div class="form-group">
            <label class="col-md-12">
                Cor <strong class="text-danger">*</strong>
            </label>
            <div class="col-md-12">
                {!! Form::select('', \App\Color::get()->pluck('select', 'id'), null, ['v-model' => 'module.color_id', 'id' => 'color', 'class' => 'form-control', 'placeholder' => 'Selecione a cor da variação']) !!}
            </div>
        </div>
    </div>

    <div class="col-lg-12">
        <div class="form-group">
            <label class="col-md-12">
                Imagem <strong class="text-danger">*</strong>
            </label>
            <div class="col-md-12">
                <img v-if="module.upload.path" v-bind:src="module.upload.path + '/' + module.upload.w" width="50" />
                {!! Form::file('', ['v-on:change' => 'm_tmp_image($event)', 'id' => 'upload-clear', 'class' => 'form-control', 'placeholder' => 'Imagem']) !!}
                {!! Form::hidden('', null, ['v-model' => 'module.upload']) !!}
            </div>
        </div>
        <div class="form-group col-md-12">
            <button class="btn btn-success" v-on:click="m_save($event)" type="button">
                <i class="fa fa-plus"></i> @{{ module.wait == false ? 'Adicionar variação' : 'Aguarde...' }}
            </button>
        </div>
    </div>
    <div class="col-md-12 table-responsive">
        <table class="table table-bordered">
            <thead>
            <tr>
                <td>Referência</td>
                <td>Cor</td>
                <td>Imagem</td>
                <td>#</td>
            </tr>
            </thead>
            <tbody>
            @if (isset($edit) && $edit->Grid->count())
                @foreach($edit->Grid as $grid)
                    @php
                        $json = [
                            'id' => $grid->id,
                            'name' => $grid->name_json,
                            'reference' => $grid->reference,
                            'upload' => $grid->upload_json,
                            'color_id' => $grid->color_id
                        ];
                    @endphp
                    <tr id="{!! $grid->id !!}">
                        <td>{!! $grid->reference !!}</td>
                        <td v-html="m_show_color('{!! $grid->color_id !!}')"></td>
                        <td>
                            @if ($grid->upload)
                                @if (file_exists(public_path('/assets/product/' . $grid->upload_json->w)))
                                    <img src="{!! url('/assets/product/' . $grid->upload_json->w) !!}" width="50" />
                                @endif
                            @endif
                        </td>
                        <td>
                            <button class="btn btn-success btn-xs" v-on:click="m_edit($event, '{!! $grid->id !!}', true)" type="button">
                                <i class="fa fa-pencil"></i>
                            </button>
                            <button class="btn btn-danger btn-xs" v-on:click="m_destroy($event, '{!! $grid->id !!}', true)" type="button">
                                <i class="fa fa-trash"></i>
                            </button>
                            <input type="hidden" v-bind:value='m_toString({!! json_encode($json) !!})' name="grids[]" />
                        </td>
                    </tr>
                @endforeach
            @endif
            <tr v-for="(k, o) in module.rows">
                <td>@{{ o.reference }}</td>
                <td>@{{ m_show_color(o.color_id) }}</td>
                <td>
                    <img v-bind:src="o.upload.path + '/' + o.upload.w" width="50" />
                </td>
                <td>
                    {{--<button class="btn btn-success btn-xs" v-on:click="m_edit($event, o.id)" type="button">--}}
                        {{--<i class="fa fa-pencil"></i>--}}
                    {{--</button>--}}
                    <button class="btn btn-danger btn-xs" v-on:click="m_destroy($event, o.id)" type="button">
                        <i class="fa fa-trash"></i>
                    </button>
                    <input type="hidden" v-bind:value="m_toString(o)" name="grids[]" />
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="col-lg-12">
        <div class="form-group col-md-12">
            <button class="btn btn-success" v-on:click="m_save_module($event)" type="button">
                <i class="fa fa-plus"></i> Adicionar módulo
            </button>
        </div>
    </div>

    <div class="col-md-12 table-responsive">
        <table class="table table-bordered">
            <thead>
            <tr>
                <td>Nome/Título</td>
                <td>Ambiente</td>
                <td>Altura</td>
                <td>Largura</td>
                <td>Profundidade</td>
                <td>#</td>
            </tr>
            </thead>
            <tbody>
            {{--@if (isset($edit) && $edit->Grid->count())--}}
                {{--@foreach($edit->Grid as $grid)--}}
                    {{--@php--}}
                        {{--$json = [--}}
                            {{--'id' => $grid->id,--}}
                            {{--'name' => $grid->name_json,--}}
                            {{--'reference' => $grid->reference,--}}
                            {{--'upload' => $grid->upload_json,--}}
                            {{--'color_id' => $grid->color_id--}}
                        {{--];--}}
                    {{--@endphp--}}
                    {{--<tr id="{!! $grid->id !!}">--}}
                        {{--<td>{!! $grid->reference !!}</td>--}}
                        {{--<td v-html="m_show_color('{!! $grid->color_id !!}')"></td>--}}
                        {{--<td>--}}
                            {{--@if ($grid->upload)--}}
                                {{--@if (file_exists(public_path('/assets/product/' . $grid->upload_json->w)))--}}
                                    {{--<img src="{!! url('/assets/product/' . $grid->upload_json->w) !!}" width="50" />--}}
                                {{--@endif--}}
                            {{--@endif--}}
                        {{--</td>--}}
                        {{--<td>--}}
                            {{--<button class="btn btn-success btn-xs" v-on:click="m_edit($event, '{!! $grid->id !!}', true)" type="button">--}}
                                {{--<i class="fa fa-pencil"></i>--}}
                            {{--</button>--}}
                            {{--<button class="btn btn-danger btn-xs" v-on:click="m_destroy($event, '{!! $grid->id !!}', true)" type="button">--}}
                                {{--<i class="fa fa-trash"></i>--}}
                            {{--</button>--}}
                            {{--<input type="hidden" v-bind:value='m_toString({!! json_encode($json) !!})' name="grids[]" />--}}
                        {{--</td>--}}
                    {{--</tr>--}}
                {{--@endforeach--}}
            {{--@endif--}}
            <tr v-for="(k, o) in module.row_modules">
                <td>@{{ m_show_name(o.name) }}</td>
                <td>@{{ o.environment }}</td>
                <td>@{{ o.height }}</td>
                <td>@{{ o.width }}</td>
                <td>@{{ o.depth }}</td>
                <td>
                    {{--<button class="btn btn-success btn-xs" v-on:click="m_edit($event, o.id)" type="button">--}}
                        {{--<i class="fa fa-pencil"></i>--}}
                    {{--</button>--}}
                    <button class="btn btn-danger btn-xs" v-on:click="m_destroy($event, o.id)" type="button">
                        <i class="fa fa-trash"></i>
                    </button>
                    <input type="hidden" v-bind:value="m_toString(o)" name="modules[]" />
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>