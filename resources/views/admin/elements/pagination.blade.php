@if ($paginator->count() > 2)
    <ul class="pagination pagination-lg m-b-0">
        <li{{ ($paginator->currentPage() == 1) ? ' class=disabled' : '' }}>
            <a href="{{ ($paginator->currentPage() == 1) ? '#' : $paginator->url(1) }}">
                <i class="fa fa-angle-left"></i>
            </a>
        </li>

        @for ($i = 1; $i <= $paginator->lastPage(); $i++)
            <li{{ ($paginator->currentPage() == $i) ? ' class=active' : '' }}>
                <a href="{{ $paginator->url($i) }}">
                    {!! $i !!}
                </a>
            </li>
        @endfor

        <li{{ ($paginator->currentPage() == $paginator->lastPage()) ? ' class=disabled' : '' }}>
            <a href="{{ ($paginator->currentPage() == $paginator->lastPage()) ? '#' : $paginator->url($paginator->currentPage()+1) }}">
                <i class="fa fa-angle-right"></i>
            </a>
        </li>
    </ul>
@endif