<!-- Left navbar-header -->
<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse slimscrollsidebar">
        <ul class="nav" id="side-menu">
            <li>
                <a href="{!! route('user.index') !!}" class="waves-effect">
                    <span class="hide-menu">Meu Perfil / Representantes</span>
                </a>
            </li>
            <li>
                <a href="{!! route('lead.index') !!}" class="waves-effect">
                    <span class="hide-menu">Contato</span>
                </a>
            </li>
            <li>
                <a href="{!! route('line.index') !!}" class="waves-effect">
                    <span class="hide-menu">Linhas</span>
                </a>
            </li>
            <li>
                <a href="{!! route('product.index') !!}" class="waves-effect">
                    <span class="hide-menu">Gerenciar Módulos</span>
                </a>
            </li>
            <li>
                <a href="{!! route('blog.index') !!}" class="waves-effect">
                    <span class="hide-menu">Novidades</span>
                </a>
            </li>
        </ul>
    </div>
</div>
<!-- Left navbar-header end -->