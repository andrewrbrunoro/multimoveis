@extends('admin')

@section('sn')
    Leads
@endsection

@section('b')
    <li class="active">
        Leads
    </li>
@endsection

@section('act')
    <div class="col-lg-2 col-sm-4 col-xs-12">
        <a href="{!! route('dashboard') !!}" class="btn btn-block btn-warning">
            <i class="fa fa-arrow-left"></i> Voltar
        </a>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="white-box">
                {!! Form::open(['route' => ['lead.index', old('name')], 'method' => 'GET']) !!}
                <div class="row">
                    <div class="form-group{!! $errors->first('name') ? ' has-error' : '' !!}">
                        <div class="col-lg-10">
                            {!! Form::text('name', request()->has('name') ? request('name') : null, ['class' => 'form-control', 'placeholder' => 'Procurar por nome']) !!}
                            {!! $errors->first('name', '<span class="help-block">:message</span>') !!}
                        </div>
                        <div class="col-lg-2">
                            <button type="submit" class="btn btn-block btn-primary">
                                <i class="fa fa-search"></i> Procurar
                            </button>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    <div class="row el-element-overlay">
        @if (isset($rows) && $rows->count())
            <div class="col-lg-12">
                <div class="white-box">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <td>Nome</td>
                            <td>E-mail</td>
                            <td>Telefone</td>
                            <td>Tipo de Contato</td>
                            <td>Status</td>
                            <td>#Opções</td>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($rows as $row)
                            <tr>
                                <td>{!! $row->name !!}</td>
                                <td>{!! $row->email !!}</td>
                                <td>{!! $row->phone !!}</td>
                                <td>
                                    {!! $row->type !!}
                                </td>
                                <td>
                                    {!! $row->closed ? '<label class="label label-danger">Aguardando resposta</label>' : '<label class="label label-success">Fechado</label>' !!}
                                </td>
                                <td>
                                    {!! Form::open(['route' => ['lead.update', $row->id], 'method' => 'PATCH']) !!}
                                    <button class="btn btn-xs btn-success" type="submit">
                                        {!! $row->closed ? 'Não Fechado' : 'Abrir novamente' !!}
                                    </button>
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        @endif
    </div>
    @if (isset($rows))
        <div class="row">
            <div class="col-lg-12">
                @include('admin.elements.pagination', ['paginator' => $rows])
            </div>
        </div>
    @endif
@endsection
