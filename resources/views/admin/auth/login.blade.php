<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="msapplication-tap-highlight" content="no"/>
    <link rel="icon" type="image/x-icon" href="{!! url('favicon.ico') !!}" />
    <title>Autenticação - {!! setting('project') !!}</title>
    <link href='http://fonts.googleapis.com/css?family=Roboto:300,400,500' rel='stylesheet' type='text/css'>
    {!! Html::style('/plugins/uikit/uikit.almost-flat.min.css', ['media' => 'all']) !!}
    {!! Html::style('/plugins/metroNotification/MetroNotificationStyle.min.css', ['media' => 'all']) !!}
    {!! Html::style('/css/login_page.min.css', ['media' => 'all']) !!}
</head>

<body class="login_page">

<canvas style="position: fixed; height: 800px;"></canvas>

<div class="login_page_wrapper">

    @include('admin.auth.alert')

    <div class="md-card" id="login_card">
        <div class="md-card-content large-padding" id="login_form">

            <div class="login_heading" title="Jorge Bart - Área administrativa">
                <div class="user_avatar"></div>
            </div>

            {!! Form::open(['route' => 'auth','id' => 'login-form']) !!}
            <div class="uk-form-row">
                <label for="login_username">E-mail/Usuário</label>
                {!! Form::text('email', null, ['class' => 'md-input', 'required' => 'required']) !!}
                {!! $errors->first('email', '<span class="uk-text-danger">:message</span>') !!}
            </div>
            <div class="uk-form-row">
                <label for="login_password">Senha</label>
                {!! Form::password('password', ['class' => 'md-input']) !!}
                {!! $errors->first('password', '<span class="uk-text-danger">:message</span>') !!}
            </div>
            <div class="uk-margin-medium-top">
                <button type="submit" class="md-btn md-btn-primary md-btn-block md-btn-large" data-uk-tooltip="{pos:'bottom'}" title="Clique para acessar">
                    ACESSAR
                </button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

{!! Html::script('/plugins/altair/common.min.js') !!}
{!! Html::script('/plugins/altair/uikit_custom.min.js') !!}
{!! Html::script('/plugins/altair/bootstrap.min.js') !!}
{!! Html::script('/plugins/altair/altair_admin_common.min.js') !!}
{!! Html::script('/plugins/altair/MetroNotification.min.js') !!}
{!! Html::script('/plugins/altair/constellation.js') !!}
{!! Html::script('/plugins/altair/login.min.js') !!}

</body>

</html>
