@if ($errors->count())
  <div class="uk-alert uk-alert-danger" data-uk-alert>
        <a href="#" class="uk-alert-close uk-close"></a>
        <ul>
            @foreach ($errors->all() as $error)
            <li>{!! $error !!}</li>
          @endforeach
        </ul>
    </div>
@endif
@if(session()->has('success'))
  <div class="uk-alert uk-alert-success" data-uk-alert>
        <a href="#" class="uk-alert-close uk-close"></a>
    {!! session('success') !!}
    </div>
@elseif(session()->has('info'))
  <div class="uk-alert uk-alert-info" data-uk-alert>
        <a href="#" class="uk-alert-close uk-close"></a>
    {!! session('info') !!}
    </div>
@elseif(session()->has('warning'))
  <div class="uk-alert uk-alert-warning" data-uk-alert>
        <a href="#" class="uk-alert-close uk-close"></a>
    {!! session('warning') !!}
    </div>
@elseif(session()->has('error'))
  <div class="uk-alert uk-alert-danger" data-uk-alert>
        <a href="#" class="uk-alert-close uk-close"></a>
    {!! session('error') !!}
    </div>
@endif