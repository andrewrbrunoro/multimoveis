@extends('admin')

@section('sn')
    Usuários
@endsection

@section('b')
    <li class="active">
        Usuários
    </li>
@endsection

@section('act')
    <div class="col-lg-3 col-sm-4 col-xs-12">
        <a href="{!! route('user.create') !!}" class="btn btn-block btn-brown" title="Clique aqui para adicionar um novo usuário">
            <i class="fa fa-plus"></i> Adicionar Usuário
        </a>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="white-box">
                {!! Form::open(['route' => ['user.index', old('name'), old('email')], 'method' => 'GET']) !!}
                <div class="row">
                    <div class="form-group{!! $errors->first('name') ? ' has-error' : '' !!}">
                        <div class="col-lg-5">
                            {!! Form::text('name', request()->has('name') ? request('name') : null, ['class' => 'form-control', 'placeholder' => 'Procurar por nome']) !!}
                            {!! $errors->first('name', '<span class="help-block">:message</span>') !!}
                        </div>
                        <div class="col-lg-5">
                            {!! Form::text('email', request()->has('email') ? request('email') : null, ['class' => 'form-control', 'placeholder' => 'Procurar por e-mail']) !!}
                            {!! $errors->first('email', '<span class="help-block">:message</span>') !!}
                        </div>
                        <div class="col-lg-2">
                            <button type="submit" class="btn btn-block btn-primary">
                                <i class="fa fa-search"></i> Procurar
                            </button>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    <div class="row el-element-overlay">
        @if (isset($rows) && $rows->count())
            @foreach($rows as $row)
                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <div class="white-box">
                        <div class="el-card-item">
                            {!! Form::open(['route' => ['user.destroy', $row->email], 'method' => 'DELETE', 'class' => 'pull-left']) !!}
                            <button type="submit" class="btn btn-danger" title="Deletar o usuário {!! $row->name !!} {!! $row->last_name !!}">
                                <i class="fa fa-trash"></i>
                            </button>
                            {!! Form::close() !!}
                            <a class="btn btn-success pull-left" title="Editar o usuário {!! $row->name !!} {!! $row->last_name !!}" href="{!! route('user.edit', $row->email) !!}">
                                <i class="fa fa-pencil"></i>
                            </a>
                            <div class="el-card-avatar el-overlay-1">
                                {!! Html::image('/images/users/abstract-user-flat-1.svg', 'User name', ['title' => 'User name']) !!}
                                <div class="el-overlay">
                                    <ul class="el-info">
                                        <li>
                                            <a class="btn default btn-outline image-popup-vertical-fit" title="Editar o usuário {!! $row->name !!} {!! $row->last_name !!}" href="{!! route('user.edit', $row->email) !!}">
                                                <i class="fa fa-pencil"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a class="btn default btn-outline image-popup-vertical-fit" href="../plugins/images/users/5.jpg" title="Histórico">
                                                <i class="icon-magnifier"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a class="btn default btn-outline" href="{!! route('user.edit', $row->email) !!}" title="Quantidade de endereços">
                                                <i class="fa fa-home"></i> {!! $row->UserAddress->count() !!}
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="el-card-content">
                                <h3 class="box-title">
                                    {!! $row->name !!} {!! $row->last_name !!}
                                </h3>
                                <small>
                                    <a href="mailto:{!! $row->email !!}" title="Enviar e-mail para {!! $row->email !!}">
                                        <i class="fa fa-send"></i> {!! $row->email !!}
                                    </a>
                                </small>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        @endif
    </div>
    @if (isset($rows))
        <div class="row">
            <div class="col-lg-12">
                @include('admin.elements.pagination', ['paginator' => $rows])
            </div>
        </div>
    @endif
@endsection