@extends('admin')

@section('sn')
    Novo usuário
@endsection

@section('b')
    <li>
        <a href="{!! route('user.index') !!}">
            Usuários
        </a>
    </li>
    <li class="active">
        Novo usuário
    </li>
@endsection

@section('act')
    <div class="col-lg-2 col-sm-4 col-xs-12">
        <a href="{!! route('user.index') !!}" class="btn btn-block btn-warning">
            <i class="fa fa-arrow-left"></i> Voltar
        </a>
    </div>
@endsection

@section('content')
    {!! Form::open(['route' => 'user.store', 'class' => 'form-material form-horizontal']) !!}
        @include('admin.user.form')
    {!! Form::close() !!}
@endsection