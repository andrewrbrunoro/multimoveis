@extends('admin')

@section('sn')
    Editar usuário <strong>{!! $user->name !!}</strong>
@endsection

@section('b')
    <li>
        <a href="{!! route('user.index') !!}">
            Usuários
        </a>
    </li>
    <li class="active">
        Editar usuário <strong>{!! $user->name !!}</strong>
    </li>
@endsection

@section('act')
    <div class="col-lg-2 col-sm-4 col-xs-12">
        <a href="{!! route('user.index') !!}" class="btn btn-block btn-warning">
            <i class="fa fa-arrow-left"></i> Voltar
        </a>
    </div>
    <div class="col-lg-2 col-sm-4 col-xs-12">
        <a href="{!! route('user.create') !!}" class="btn btn-block btn-success" title="Clique aqui para adicionar um novo usuário">
            <i class="fa fa-plus"></i> Adicionar Usuário
        </a>
    </div>
@endsection

@section('content')
    {!! Form::model($user, ['route' => ['user.update', $user->email], 'class' => 'form-material form-horizontal', 'method' => 'PATCH']) !!}
    @include('admin.user.form')
    {!! Form::close() !!}
@endsection