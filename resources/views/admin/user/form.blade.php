<div class="row">
    <div class="col-sm-12">
        <div class="white-box">
            <h3 class="box-title">Informações do usuário</h3>
            <div class="row">
                <div class="col-lg-4">
                    <div class="form-group{!! $errors->first('name') ? ' has-error' : '' !!}">
                        <label class="col-md-12" for="name">
                            Nome <strong class="text-danger">*</strong>
                        </label>
                        <div class="col-md-12">
                            {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Exemplo: João, João Fernandes']) !!}
                            {!! $errors->first('name', '<span class="help-block">:message</span>') !!}
                        </div>
                    </div>
                    <div class="form-group{!! $errors->first('email') ? ' has-error' : '' !!}">
                        <label class="col-md-12" for="email">
                            E-mail <strong class="text-danger">*</strong>
                        </label>
                        <div class="col-md-12">
                            {!! Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'Exemplo: fulano@gmail.com']) !!}
                            {!! $errors->first('email', '<span class="help-block">:message</span>') !!}
                        </div>
                    </div>
                    <div class="form-group{!! $errors->first('type') ? ' has-error' : '' !!}">
                        <label class="col-md-12" for="type">
                            Tipo de Usuário <strong class="text-danger">*</strong>
                        </label>
                        <div class="col-md-12">
                            {!! Form::select('type', ['0' => 'Usuário', '1' => 'Representante'], 0, ['class' => 'form-control']) !!}
                            {!! $errors->first('type', '<span class="help-block">:message</span>') !!}
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group{!! $errors->first('last_name') ? ' has-error' : '' !!}">
                        <label class="col-md-12" for="last_name">
                            Sobrenome <strong class="text-danger">*</strong>
                        </label>
                        <div class="col-md-12">
                            {!! Form::text('last_name', null, ['class' => 'form-control', 'placeholder' => 'Exemplo: Silva, Silva da Silva']) !!}
                            {!! $errors->first('last_name', '<span class="help-block">:message</span>') !!}
                        </div>
                    </div>
                    <div class="form-group{!! $errors->first('password') ? ' has-error' : '' !!}">
                        <label class="col-md-12" for="password">
                            Senha <strong class="text-info">(Opcional)</strong>
                        </label>
                        <div class="col-md-12">
                            {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Senha']) !!}
                            {!! $errors->first('password', '<span class="help-block">:message</span>') !!}
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group{!! $errors->first('phone') ? ' has-error' : '' !!}">
                        <label class="col-md-12" for="phone">
                            Telefone/Celular <strong class="text-danger">*</strong>
                        </label>
                        <div class="col-md-12">
                            {!! Form::text('phone', null, ['class' => 'form-control', 'placeholder' => 'Exemplo: (99) 99999-9999']) !!}
                            {!! $errors->first('phone', '<span class="help-block">:message</span>') !!}
                        </div>
                    </div>
                    <div class="form-group{!! $errors->first('password_confirmation') ? ' has-error' : '' !!}">
                        <label class="col-md-12" for="password_confirm">
                            Repetir Senha <strong class="text-info">(Opcional)</strong>
                        </label>
                        <div class="col-md-12">
                            {!! Form::password('password_confirmation', ['class' => 'form-control', 'placeholder' => 'Repetir Senha']) !!}
                            {!! $errors->first('password_confirmation', '<span class="help-block">:message</span>') !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row" id="addresses" exist='{!! isset($user) && $user->UserAddress->count() ? (session()->has('addresses')) ? json_encode(session('addresses')) : $user->UserAddress : '' !!}'>
    <div class="col-sm-6">
        <div class="white-box">
            <div v-show="address_error != 0" class="alert alert-danger" id="address-error">
                <p v-for="(key, o) in address_error">
                    @{{ o[0] }}
                </p>
            </div>
            <h3 class="box-title">Endereço</h3>
            <p>preencha o cep e automaticamente irá preencher os campos.</p>
            <div class="form-group{!! $errors->first('zip_code') ? ' has-error' : '' !!}">
                <label class="col-md-12" for="zip_code">
                    CEP
                </label>
                <div class="col-md-12">
                    {!! Form::text('', null, ['autocomplete' => 'off', 'v-model' => 'zip_code', 'class' => 'form-control', 'placeholder' => 'Exemplo: 9999-999, Rota 52']) !!}
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group{!! $errors->first('street') ? ' has-error' : '' !!}">
                        <label class="col-md-12" for="street">
                            Rua
                        </label>
                        <div class="col-md-12">
                            {!! Form::text('', null, ['autocomplete' => 'off', 'v-model' => 'street', 'class' => 'form-control', 'placeholder' => 'Exemplo: Av, Rua']) !!}
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group{!! $errors->first('street_number') ? ' has-error' : '' !!}">
                        <label class="col-md-12" for="street_number">
                            Número
                        </label>
                        <div class="col-md-12">
                            {!! Form::text('', null, ['autocomplete' => 'off', 'v-model' => 'street_number', 'class' => 'form-control', 'placeholder' => 'Número']) !!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group{!! $errors->first('district') ? ' has-error' : '' !!}">
                <label class="col-md-12" for="district">
                    Bairro <strong class="text-danger">*</strong>
                </label>
                <div class="col-md-12">
                    {!! Form::text('', null, ['autocomplete' => 'off', 'v-model' => 'district', 'class' => 'form-control', 'placeholder' => 'Bairro']) !!}
                </div>
            </div>
            <div class="form-group{!! $errors->first('city') ? ' has-error' : '' !!}">
                <label class="col-md-12" for="city">
                    Cidade <strong class="text-danger">*</strong>
                </label>
                <div class="col-md-12">
                    {!! Form::text('', null, ['autocomplete' => 'off', 'v-model' => 'city', 'class' => 'form-control', 'placeholder' => 'Cidade']) !!}
                </div>
            </div>
            <div class="form-group{!! $errors->first('state') ? ' has-error' : '' !!}">
                <label class="col-md-12" for="state">
                    Estado <strong class="text-danger">*</strong>
                </label>
                <div class="col-md-12">
                    {!! Form::text('', null, ['autocomplete' => 'off', 'v-model' => 'state', 'class' => 'form-control', 'placeholder' => 'Estado']) !!}
                </div>
            </div>
            <div class="form-group{!! $errors->first('country') ? ' has-error' : '' !!}">
                <label class="col-md-12" for="country">
                    País <strong class="text-danger">*</strong>
                </label>
                <div class="col-md-12">
                    {!! Form::text('', null, ['autocomplete' => 'off', 'v-model' => 'country', 'class' => 'form-control', 'placeholder' => 'País']) !!}
                </div>
            </div>
            <div class="form-group">
                <button class="btn btn-success" type="button" role="button" v-on:click="validate">
                    Adicionar Endereço
                </button>
            </div>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="white-box">
            <h3 class="box-title">Endereços cadastrados</h3>
            <ul class="list-style-none">
                <input type="hidden" name="remove_addresses" v-model="itemsRemove" />
                <li v-for="(key, o) in addresses">
                    <div class="-quote-left">
                        @{{ o.street }}, @{{ o.street_number }} <br/>
                        @{{ o.district }} <br/>
                        @{{ o.city }}/@{{ o.state }} - @{{ o.country }}
                        <input type="hidden" v-bind:value="o.toJson" name="addresses[]"/>
                        <div class="form-horizontal">
                            <button class="btn btn-xs btn-success" v-on:click="edit($event, key)" type="button">
                                <i class="fa fa-pencil"></i>
                            </button>
                            <button class="btn btn-xs btn-danger" v-on:click="destroy($event, key)"  type="button">
                                <i class="fa fa-trash"></i>
                            </button>
                        </div>
                    </div>
                    <hr />
                </li>
            </ul>
        </div>
    </div>

</div>

<hr />

<div class="col-lg-12">
    <div class="form-group">
        <button class="btn btn-primary" type="submit">
            Salvar Usuário
        </button>
    </div>
</div>

@section('js')
    {!! Html::script('/js/components/addresses.js') !!}
@endsection