<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>PDF Multimóveis</title>


    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet" />
    <link rel="stylesheet" href="{!! url('/assets/css/lib/bootstrap.min.css') !!}" />
    <link rel="stylesheet" href="{!! url('/assets/css/lib/owl.carousel.min.css') !!}" />
    <link rel="stylesheet" href="{!! url('/assets/css/main.css') !!}" />
<body>


@yield('content')


<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="{!! url('/assets/js/lib/jquery.min.js') !!}"></script>
<script src="{!! url('/assets/js/bootstrap.min.js') !!}"></script>
<script type="text/javascript" src="{!! url('/assets/js/lib/owl.carousel.min.js') !!}"></script>
<script type="text/javascript" src="{!! url('/assets/js/lib/owl.carousel.thumbs.min.js') !!}"></script>
<script type="text/javascript" src="{!! url('/assets/js/lib/jquery.elevateZoom-3.0.8.min.js') !!}"></script>
<script type="text/javascript" src="{!! url('/assets/js/main.js') !!}"></script>


</body>
</html>
