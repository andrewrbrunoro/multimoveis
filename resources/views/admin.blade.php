<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="/images/favicon.png">
    <title>{!! setting('project') !!} - Área administrativa</title>
    <!-- Bootstrap Core CSS -->
    {!! Html::style('/plugins/bootstrap/css/bootstrap.css') !!}
    <!-- Menu CSS -->
    {!! Html::style('/plugins/sidebar/sidebar-nav.min.css') !!}
    <!-- toastr -->
    {!! Html::style('/plugins/toastr/toastr.min.css') !!}
    <!-- SWEETALERT -->
    {!! Html::style('/plugins/sweetalert/sweetalert.css') !!}
    <!-- Morris CSS -->
    {!! Html::style('/plugins/morrisjs/morris.css') !!}
    <!-- animation CSS -->
    {!! Html::style('/css/elite/animate.css') !!}
    @yield('css')
    <!-- Custom CSS -->
    {!! Html::style('/css/elite/style.css') !!}
    <!-- color CSS -->
    {!! Html::style('/css/elite/colors/blue.css') !!}
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script>
        window.Laravel = {
            token: '{!! csrf_token() !!}',
            url: '{!! url('/') !!}'
        };
        @if (session()->has('path'))
            window.Laravel.path = '{!! session('path') !!}';
        @endif
    </script>
</head>
<body languages='{!! setting('languages') !!}'>

<!-- Preloader -->
<div class="preloader" style="display: none;">
    <div class="cssload-speeding-wheel"></div>
</div>
<div id="wrapper">
    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-static-top m-b-0">
        <div class="navbar-header navbar-brown"> <a class="navbar-toggle hidden-sm hidden-md hidden-lg " href="javascript:void(0)" data-toggle="collapse" data-target=".navbar-collapse"><i class="ti-menu"></i></a>
            <div class="top-left-part">
                <a class="logo" href="{!! route('dashboard') !!}">
                    <b>
                        <img src="{!! url('/images/client/logo_topo.png') !!}" alt="Multi Móveis" style="margin-left: 50%;" />
                    </b>
                </a>
            </div>
            <ul class="nav navbar-top-links navbar-right pull-right">

                {{--@include('admin.elements.notifications')--}}

                {{--@include('admin.elements.progress')--}}

                <li class="dropdown">
                    <a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#">
                        <img src="/images/users/1.jpg" alt="user-img" width="36" class="img-circle">
                        <b class="hidden-xs">{!! auth()->user()->name !!}  <i class="fa fa-caret-down"></i></b>
                    </a>
                    <ul class="dropdown-menu dropdown-user animated flipInY">
                        @include('admin.elements.user')
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
        </div>
        <!-- /.navbar-header -->
        <!-- /.navbar-top-links -->
        <!-- /.navbar-static-side -->
    </nav>
    <!-- Left navbar-header -->
    @include('admin.elements.menu')
    <!-- Left navbar-header end -->
    <!-- Page Content -->
    <div id="page-wrapper" style="min-height: 124px;">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">
                        @yield('sn')
                    </h4>
                </div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    <ol class="breadcrumb">
                        <li>
                            <a href="{!! route('line.index') !!}">Linhas</a>
                        </li>
                        @yield('b')
                    </ol>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            @if (session()->has('error'))
                <div class="alert alert-danger">
                    {!! session('error') !!}
                </div>
            @elseif(session()->has('info'))
                <div class="alert alert-info">
                    {!! session('info') !!}
                </div>
            @elseif(session()->has('success'))
                <div class="alert alert-success">
                    {!! session('success') !!}
                </div>
            @endif
            <div class="row">
                @yield('act')
            </div>
            <hr />
            @yield('content')
        </div>
        <!-- /.container-fluid -->
        <footer class="footer text-center"> 2016 © Elite Admin brought to you by themedesigner.in </footer>
    </div>
    <!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->


<!-- jQuery -->
{!! Html::script('/plugins/jquery/jquery.min.js') !!}
<!-- Bootstrap Core JavaScript -->
{!! Html::script('/plugins/bootstrap/js/bootstrap.min.js') !!}
<!-- VUE JS -->
{!! Html::script('/plugins/vue/vue.min.js') !!}
<!-- VUE RESOURCE -->
{!! Html::script('/plugins/vue/vue-resource.js') !!}
<!-- toastr -->
{!! Html::script('/plugins/toastr/toastr.min.js') !!}
<!-- SWEETALERT -->
{!! Html::script('/plugins/sweetalert/sweetalert.min.js') !!}
<!-- Menu Plugin JavaScript -->
{!! Html::script('/plugins/sidebar/sidebar-nav.min.js') !!}
<!--slimscroll JavaScript -->
{!! Html::script('/js/elite/jquery.slimscroll.js') !!}
<!--Wave Effects -->
{!! Html::script('/js/elite/waves.js') !!}
<!--Morris JavaScript -->
{{--{!! Html::script('/plugins/raphael/raphael-min.js') !!}--}}
{{--{!! Html::script('/plugins/morrisjs/morris.js') !!}--}}
<!-- Sparkline chart JavaScript -->
{{--{!! Html::script('/plugins/jquery-sparkline/jquery.sparkline.min.js') !!}--}}
{{--{!! Html::script('/plugins/jquery-sparkline/jquery.charts-sparkline.js') !!}--}}
<!-- Custom Theme JavaScript -->
{!! Html::script('/js/common.js') !!}
@yield('js')
{!! Html::script('/js/elite/custom.min.js') !!}
</body>
</html>