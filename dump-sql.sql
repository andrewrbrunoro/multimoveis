/*
SQLyog Enterprise v12.13 (64 bit)
MySQL - 10.1.9-MariaDB-log : Database - multi
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`multi` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `multi`;

/*Table structure for table `colors` */

DROP TABLE IF EXISTS `colors`;

CREATE TABLE `colors` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` longtext,
  `value` varchar(255) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

/*Data for the table `colors` */

insert  into `colors`(`id`,`name`,`value`,`deleted_at`,`created_at`,`updated_at`) values (1,'[{\"name\": \"Branco Brilho\", \"language\": \"portugues\"}]',NULL,NULL,'2017-03-20 22:12:04','2017-03-20 22:12:04'),(2,'[{\"name\": \"Branco / Cinza Brilho\", \"language\": \"portugues\"}]',NULL,NULL,'2017-03-20 22:12:47','2017-03-20 22:12:47'),(3,'[{\"name\": \"Cinza\", \"language\": \"portugues\"}]',NULL,NULL,'2017-03-20 22:13:02','2017-03-20 22:13:02'),(4,'[{\"name\": \"Branco / Preto Brilho\", \"language\": \"portugues\"}]',NULL,NULL,'2017-03-20 22:13:14','2017-03-20 22:13:14'),(5,'[{\"name\": \"Preto\", \"language\": \"portugues\"}]',NULL,NULL,'2017-03-20 22:13:22','2017-03-20 22:13:22'),(6,'[{\"name\": \"Branco / Azul Brilho\", \"language\": \"portugues\"}]',NULL,NULL,'2017-03-20 22:16:55','2017-03-20 22:16:55'),(7,'[{\"name\": \"Branco / Rosa Brilho\", \"language\": \"portugues\"}]',NULL,NULL,'2017-03-20 22:17:02','2017-03-20 22:17:02'),(8,'[{\"name\": \"Branco / Lilás Brilho\", \"language\": \"portugues\"}]',NULL,NULL,'2017-03-20 22:17:10','2017-03-20 22:17:10'),(9,'[{\"name\": \"Branco Acetinado\", \"language\": \"portugues\"}]',NULL,NULL,'2017-03-20 22:17:23','2017-03-20 22:17:23'),(10,'[{\"name\": \"Branco / Colorido\", \"language\": \"portugues\"}]',NULL,NULL,'2017-03-20 22:17:32','2017-03-20 22:17:32');

/*Table structure for table `environments` */

DROP TABLE IF EXISTS `environments`;

CREATE TABLE `environments` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `line_id` int(10) DEFAULT NULL,
  `color_id` int(10) DEFAULT NULL,
  `name` longtext NOT NULL,
  `reference` varchar(255) DEFAULT NULL,
  `upload` longtext,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `environments` */

insert  into `environments`(`id`,`line_id`,`color_id`,`name`,`reference`,`upload`,`deleted_at`,`created_at`,`updated_at`) values (1,1,1,'{\"portugues\":\"Idioma portugu\\u00eas\"}','ref1','{\"name\":\"taxi-jaguaruna-banner.jpg\",\"interna\":\"1491453286-1024x294-taxi-jaguaruna-banner.jpg\",\"home\":\"1491453286-780x340-taxi-jaguaruna-banner.jpg\",\"w\":\"1491453287-50x50-taxi-jaguaruna-banner.jpg\",\"path\":\"http:\\/\\/multi.dev\\/assets\\/environment\"}',NULL,'2017-04-06 01:34:50','2017-04-06 01:34:50'),(2,1,5,'{\"portugues\":\"Ambiente 2\"}','amb-2','{\"name\":\"TAXI.jpg\",\"interna\":\"1491453383-1024x294-TAXI.jpg\",\"home\":\"1491453383-780x340-TAXI.jpg\",\"w\":\"1491453383-50x50-TAXI.jpg\",\"path\":\"http:\\/\\/multi.dev\\/assets\\/environment\"}',NULL,'2017-04-06 01:36:26','2017-04-06 01:36:26');

/*Table structure for table `grids` */

DROP TABLE IF EXISTS `grids`;

CREATE TABLE `grids` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `product_id` int(10) DEFAULT NULL,
  `color_id` int(10) DEFAULT NULL,
  `reference` varchar(60) DEFAULT NULL,
  `upload` longtext,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `grids` */

insert  into `grids`(`id`,`product_id`,`color_id`,`reference`,`upload`,`deleted_at`,`created_at`,`updated_at`) values (1,1,1,'teste','{\"name\":\"taxi-jaguaruna-banner.jpg\",\"interna\":\"1491457284-1024x294-taxi-jaguaruna-banner.jpg\",\"home\":\"1491457284-780x340-taxi-jaguaruna-banner.jpg\",\"w\":\"1491457284-50x50-taxi-jaguaruna-banner.jpg\",\"path\":\"http:\\/\\/multi.dev\\/assets\\/environment\"}',NULL,'2017-04-06 02:41:27','2017-04-06 02:41:27'),(2,1,1,'Descrição','{\"name\":\"taxi-jaguaruna-banner.jpg\",\"interna\":\"1491457321-1024x294-taxi-jaguaruna-banner.jpg\",\"home\":\"1491457321-780x340-taxi-jaguaruna-banner.jpg\",\"w\":\"1491457321-50x50-taxi-jaguaruna-banner.jpg\",\"path\":\"http:\\/\\/multi.dev\\/assets\\/environment\"}',NULL,'2017-04-06 02:42:15','2017-04-06 02:42:15'),(3,1,3,'var-2','{\"name\":\"taxi-jaguaruna-banner.jpg\",\"interna\":\"1491457334-1024x294-taxi-jaguaruna-banner.jpg\",\"home\":\"1491457334-780x340-taxi-jaguaruna-banner.jpg\",\"w\":\"1491457334-50x50-taxi-jaguaruna-banner.jpg\",\"path\":\"http:\\/\\/multi.dev\\/assets\\/environment\"}',NULL,'2017-04-06 02:42:15','2017-04-06 02:42:15');

/*Table structure for table `leads` */

DROP TABLE IF EXISTS `leads`;

CREATE TABLE `leads` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `type` varchar(60) DEFAULT NULL,
  `name` varchar(120) DEFAULT NULL,
  `email` varchar(120) DEFAULT NULL,
  `phone` varchar(30) DEFAULT NULL,
  `message` longtext,
  `closed` tinyint(1) DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `leads` */

insert  into `leads`(`id`,`type`,`name`,`email`,`phone`,`message`,`closed`,`deleted_at`,`created_at`,`updated_at`) values (1,'contato','ANDREW RODRIGUES BRUNORO','andrew.rodrigues@fatordigital.com.br','5194825959',NULL,0,NULL,'2017-04-06 02:00:27','2017-04-06 02:00:27');

/*Table structure for table `lines` */

DROP TABLE IF EXISTS `lines`;

CREATE TABLE `lines` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `descriptions` longtext,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `lines` */

insert  into `lines`(`id`,`name`,`slug`,`descriptions`,`deleted_at`,`created_at`,`updated_at`) values (1,'linha 1','linha-1','{\"portugues\":\"Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam.\",\"ingles\":\"\",\"espanhol\":\"\"}',NULL,'2017-04-06 01:34:50','2017-04-06 01:34:50');

/*Table structure for table `migrations` */

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `migrations` */

insert  into `migrations`(`id`,`migration`,`batch`) values (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1);

/*Table structure for table `password_resets` */

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `password_resets` */

/*Table structure for table `posts` */

DROP TABLE IF EXISTS `posts`;

CREATE TABLE `posts` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `views` int(10) DEFAULT '0',
  `resume` text,
  `alternative_url` text,
  `content` longtext,
  `status` tinyint(1) DEFAULT '1',
  `filename` longtext,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `posts` */

insert  into `posts`(`id`,`slug`,`title`,`views`,`resume`,`alternative_url`,`content`,`status`,`filename`,`deleted_at`,`created_at`,`updated_at`) values (1,'teste-1','Teste 1',0,'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.',NULL,'<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.<br></p>',1,NULL,NULL,'2017-04-06 02:44:05','2017-04-06 02:44:05');

/*Table structure for table `products` */

DROP TABLE IF EXISTS `products`;

CREATE TABLE `products` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `environment_id` int(10) DEFAULT '0',
  `name` longtext,
  `descriptions` longtext,
  `height` varchar(10) DEFAULT '0',
  `width` varchar(10) DEFAULT '0',
  `depth` varchar(10) DEFAULT '0',
  `video` varchar(255) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `products` */

insert  into `products`(`id`,`environment_id`,`name`,`descriptions`,`height`,`width`,`depth`,`video`,`deleted_at`,`created_at`,`updated_at`) values (1,0,'{\"portugues\":\"Ber\\u00e7os\",\"ingles\":\"\",\"espanhol\":\"\"}','{\"portugues\":\"Descri\\u00e7\\u00e3o\",\"ingles\":\"\",\"espanhol\":\"\"}','0.00','0.00','0.00',NULL,NULL,'2017-04-06 02:42:15','2017-04-06 02:42:15');

/*Table structure for table `settings` */

DROP TABLE IF EXISTS `settings`;

CREATE TABLE `settings` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) NOT NULL,
  `value` longtext,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `settings` */

insert  into `settings`(`id`,`key`,`value`,`created_at`,`updated_at`) values (1,'course-tabs','[{\"title\": \"Aprensetação\", \"key\": \"apresentacao\"}, {\"title\": \"O Concurso\", \"key\": \"o-concurso\"}, {\"title\": \"Sobre\", \"key\": \"sobre\"}]','2017-03-15 21:02:11','2017-03-09 00:09:52'),(2,'languages','[{\"name\": \"Português\", \"slug\": \"portugues\"},{\"name\": \"Inglês\", \"slug\": \"ingles\"},{\"name\": \"Espanhol\", \"slug\": \"espanhol\"}]','2017-03-17 21:02:40','2017-03-17 19:33:42'),(3,'project','MultiMóveis','2017-03-18 02:20:17','2017-03-18 02:20:19'),(4,'contact','andrewrbrunoro@gmail.com','2017-04-06 01:54:05','2017-04-06 01:54:04');

/*Table structure for table `user_addresses` */

DROP TABLE IF EXISTS `user_addresses`;

CREATE TABLE `user_addresses` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) NOT NULL,
  `zip_code` varchar(120) DEFAULT NULL,
  `street` varchar(255) DEFAULT NULL,
  `street_number` varchar(20) DEFAULT NULL,
  `district` varchar(255) DEFAULT NULL,
  `city` varchar(120) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `country` varchar(120) DEFAULT NULL,
  `geo_location` longtext,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `user_addresses` */

insert  into `user_addresses`(`id`,`user_id`,`zip_code`,`street`,`street_number`,`district`,`city`,`state`,`country`,`geo_location`,`deleted_at`,`created_at`,`updated_at`) values (1,1,'91040220','Rua Juruá','222','Jardim São Pedro','Porto Alegre','Rio Grande do Sul','Brasil','{\"bairro\":\"Jardim São Pedro\",\"cidade\":\"Porto Alegre\",\"cep\":\"91040220\",\"logradouro\":\"Rua Juruá\",\"estado_info\":{\"area_km2\":\"281.731,445\",\"codigo_ibge\":\"43\",\"nome\":\"Rio Grande do Sul\"},\"cidade_info\":{\"area_km2\":\"496,682\",\"codigo_ibge\":\"4314902\"},\"estado\":\"RS\"}',NULL,'2017-03-03 20:38:52','2017-03-03 20:42:58'),(2,1,'88801-660','Rua Ângelo Amboni','233','Centro','Criciúma','Santa Catarina','Brasil','{\"bairro\":\"Centro\",\"cidade\":\"Criciúma\",\"cep\":\"88801660\",\"logradouro\":\"Rua Ângelo Amboni\",\"estado_info\":{\"area_km2\":\"95.733,978\",\"codigo_ibge\":\"42\",\"nome\":\"Santa Catarina\"},\"cidade_info\":{\"area_km2\":\"235,701\",\"codigo_ibge\":\"4204608\"},\"estado\":\"SC\"}',NULL,'2017-03-03 20:41:55','2017-03-03 20:42:58'),(3,2,'88801-018','Travessa Antônio Augusto Althoff','321','Centro','Criciúma','Santa Catarina','Brasil','{\"bairro\":\"Centro\",\"cidade\":\"Criciúma\",\"logradouro\":\"Travessa Antônio Augusto Althoff\",\"estado_info\":{\"area_km2\":\"95.733,978\",\"codigo_ibge\":\"42\",\"nome\":\"Santa Catarina\"},\"cep\":\"88801018\",\"cidade_info\":{\"area_km2\":\"235,701\",\"codigo_ibge\":\"4204608\"},\"estado\":\"SC\"}','2017-03-18 02:15:31','2017-03-03 20:42:53','2017-03-18 02:15:31'),(4,3,'91040220','Rua Juruá','231','Jardim São Pedro','Porto Alegre','Rio Grande do Sul','Brasil','{\"bairro\":\"Jardim São Pedro\",\"cidade\":\"Porto Alegre\",\"cep\":\"91040220\",\"logradouro\":\"Rua Juruá\",\"estado_info\":{\"area_km2\":\"281.731,445\",\"codigo_ibge\":\"43\",\"nome\":\"Rio Grande do Sul\"},\"cidade_info\":{\"area_km2\":\"496,682\",\"codigo_ibge\":\"4314902\"},\"estado\":\"RS\"}',NULL,'2017-03-22 21:37:05','2017-03-22 21:37:05');

/*Table structure for table `user_attributes` */

DROP TABLE IF EXISTS `user_attributes`;

CREATE TABLE `user_attributes` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) NOT NULL,
  `key` varchar(20) NOT NULL,
  `value` longtext,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `user_attributes` */

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` tinyint(1) DEFAULT '0' COMMENT '1 = representante, 0 = usuário',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `users` */

insert  into `users`(`id`,`type`,`name`,`last_name`,`email`,`phone`,`password`,`remember_token`,`deleted_at`,`created_at`,`updated_at`) values (1,0,'ANDREW','RODRIGUES BRUNORO','andrewrbrunoro@gmail.com','5194825959','$2y$10$BpLxNFKda686R/XDR.guYOIFxcabNXblWpTM8QxxRLjouIpHhM5ne','rpvMW4Fqw6uZTvRFiKm47LWYrfMXCWwvJ9v465CS6nfz19HUzXdlfPdmhAWs',NULL,'2017-03-03 20:38:52','2017-03-23 21:13:53'),(2,0,'Diego','Rodrigues Brunoro','diegorbrunoro@gmail.com','51998883526','$2y$10$lGUddGHGhESVRdAhkoGe2OoDVbyiFGnaYkqigfs5RZUCkjDqUaGy.',NULL,'2017-03-18 02:15:30','2017-03-03 20:42:52','2017-03-18 02:15:30'),(3,0,'Representante','Repre','representante@representante.com.br','51994825959',NULL,NULL,NULL,'2017-03-22 21:31:59','2017-03-22 21:32:15');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
