<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class ImageEvent
{
    use SerializesModels;

    public $image, $request, $collect;

    /**
     * ImageEvent constructor.
     * @param $request
     * @param $collect
     * @param $request_key
     *
     * responsive_sizes = array (  array( w => value, h => value ) || array( w => value ) || array( h => value ) )
     * sizes = array (  array( w => value, h => value ) || array( w => value ) || array( h => value ) )
     * path = caminho que será salvo os arquivos
     * callback = função de retorno
     *
     */
    public function __construct($request, $collect, $request_key = 'file')
    {
        $this->request = $request;
        $this->image = $request->file($request_key);
        $this->collect = collect($collect);
    }
}
