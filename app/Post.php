<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{

    use SoftDeletes;

    protected $fillable = [
        'slug', 'title', 'resume',
        'content', 'alternative_url', 'status', 'filename'
    ];

    public function getUploadAttribute()
    {
        if ($this->filename != '')
            return json_decode($this->filename, true);
    }

}
