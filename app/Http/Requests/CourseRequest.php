<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CourseRequest extends FormRequest
{
    private $return = [];

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    private function course_is_patch()
    {
        if ($this->method() == 'PATCH') {
            $this->return += [
                'slug' => 'required|unique:courses,slug,' . $this->segment(2) . ',slug'
            ];
        }
    }

    private function if_exist_attributes()
    {
        if ($this->request->has('course_attributes'))
            $this->session()->flash('course_attributes', $this->request->get('course_attributes'));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $this->if_exist_attributes();
        $this->course_is_patch();
        $this->return += [
            'discipline_id' => 'required',
            'name' => 'required',
            'status' => 'required'
        ];
        return $this->return;
    }
}
