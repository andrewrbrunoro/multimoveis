<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    private $return;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    private function user_is_patch()
    {
        if ($this->method() != 'PATCH') {
            if ($this->request->get('type') == 0)
                $this->return += ['password' => 'required|min:6|confirmed', 'password_confirmation' => 'required|min:6'];

            $this->return += [
                'email' => 'required|email|unique:users'
            ];
        } else {
            $this->return += [
                'email' => 'required|email|unique:users,email,' . $this->segment(2) . ',email'
            ];
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->request->has('addresses'))
            $this->session()->flash('addresses', array_to_json($this->get('addresses')));
        $this->return = [
            'name' => 'required',
            'phone' => 'required',
            'last_name' => 'required',
        ];
        $this->user_is_patch();
        return $this->return;
    }
}
