<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LineRequest extends FormRequest
{
    private $return = [];

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    private function is_patch()
    {
        if ($this->method() == 'PATCH') {
            $this->return += [
                'slug' => 'required|unique:lines,slug,' . $this->segment(2) . ',slug'
            ];
        }
        if ($this->method() == 'POST') {
            $this->return += [
                'slug' => 'required'
            ];
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->has('environments')) {
            $this->session()->flash('environments', $this->get('environments'));
        }
        $this->is_patch();
        $this->return = [
            'name' => 'required'
        ];
        return $this->return;
    }
}
