<?php

namespace App\Http\Controllers\Api;

use App\Events\ImageEvent;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ApiController extends Controller
{

    public function zip_validate(Request $request)
    {
        $this->validate($request, [
            'zip_code' => 'required',
            'street' => 'required',
            'district' => 'required',
            'city' => 'required',
            'state' => 'required',
            'country' => 'required',
        ], [], ['zip_code' => 'CEP', 'street' => 'Rua', 'district' => 'Bairro', 'city' => 'Cidade', 'state' => 'Estado', 'country' => 'País']);
        return response()->json(['error' => false, 'msg' => 'Cep válido']);
    }

    public function tmp_image(Request $request)
    {
        if ($request->hasFile('file')) {
            event(new ImageEvent($request,
                [
                    'path' => $request->get('path'),
                    'responsive_sizes' => [
                        ['w' => '1024', 'h' => '294', 'name' => 'interna'],
                        ['w' => '780', 'h' => '340', 'name' => 'home'],
                        ['w' => '50', 'h' => '50', 'name' => 'w']
                    ]
                ]
            ));

            return response()
                ->json(['error' => false, 'msg' => 'Arquivo salvo com sucesso.',
                    'data' => $request->get('return_upload') + ['path' => url($request->get('path'))]
                ]);
        } else {
            return response()
                ->json(['error' => true, 'msg' => 'Por favor, selecione a imagem.'], 400);
        }
    }

}
