<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/panel';


    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    public function login()
    {
        if (request()->method() == 'POST') {
            if (auth()->attempt(request()->only(['email', 'password']))) {
                return redirect()->route('dashboard');
            } else {
                return redirect()
                    ->back()
                    ->with('error', 'Verifique se digitou os dados corretamente.');
            }
        }
        return view('admin.auth.login');
    }

    public function logout()
    {
        auth()->logout();
        return redirect()->route('auth')->with('success', 'Obrigado!');
    }

}
