<?php

namespace App\Http\Controllers\Site;

use App\Http\Requests\ContactRequest;
use App\Lead;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

class ContactController extends Controller
{

    public function index()
    {
        return view('site.contact');
    }

    public function store(ContactRequest $contactRequest, Lead $lead)
    {
        $contactRequest->merge(['type' => 'contato']);
        if ($lead->create($contactRequest->all())) {

            Mail::send('emails.contact', ['body' => $contactRequest->all()], function($m) {
                $m->from('no-reply@multimoveis.com.br', 'Contato Site');
                $m->to(setting('contact'), 'Contato')->subject('Contato realizado pelo site');
            });

            return redirect()->route('contact.index')
                ->with('success', 'Obrigado, entraremos em contato com vocês em breve.');
        }
        return redirect()->back()
            ->with('error', 'Ops, não foi possível enviar o formulário, tente novamente.');
    }

}
