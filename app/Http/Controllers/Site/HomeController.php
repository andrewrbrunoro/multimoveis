<?php

namespace App\Http\Controllers\Site;

use App\Line;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{

    public function index(Line $line)
    {
        $lines = $line->all();
        return view('site.index', compact('lines'));
    }

    public function about()
    {
        return view('site.about');
    }

    public function video()
    {
        return view('site.video');
    }

    public function show_line($slug, Line $line)
    {
        $show = $line->whereSlug($slug)->first();
        if (!$show)
            return redirect('/')->with('error', 'Linha não encontrada.');

        return view('site.show_line', compact('show'));
    }

}
