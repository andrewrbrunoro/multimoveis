<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct(Product $product)
    {
        $products = $product->where('environment_id', '=', 0)
            ->orWhereNull('environment_id')->get();
        view()->share(compact('products'));
    }

}
