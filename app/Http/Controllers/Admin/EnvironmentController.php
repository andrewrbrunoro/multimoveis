<?php

namespace App\Http\Controllers\Admin;

use App\Environment;
use App\Line;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EnvironmentController extends Controller
{

    public function index(Request $request)
    {
        if (!$request->has('line'))
            return redirect()->route('line.index')->with('error', trans('returns.not_exist'));

        $line = Line::find($request->get('line'));
        if (!$line)
            return redirect()->route('line.index')->with('error', trans('returns.not_exist'));

        $rows = Environment::where('line_id', '=', $request->get('line'))->orderBy('created_at', ' desc')->get();

        return view('admin.environment.index', compact('rows', 'line'));
    }

    public function create()
    {
        return view('admin.environment.create');
    }

    public function store(Request $request)
    {
        if ($request->has('environments')) {
            $environment = new Environment();
            $environment->create_customized($request->get('environments'), $request->get('line'));

            if ($request->has('line')) {
                return redirect()
                    ->route('environment.index', ['line' => $request->get('line')])
                    ->with('success', 'Ambientes criado com sucesso.');
            }
        }
        return redirect()->back()
            ->with('error', 'Tente registrar os ambientes novamente.');
    }

    public function edit($id)
    {
        return view('admin.environment.edit');
    }

    public function update($id)
    {

    }

    public function destroy($id)
    {
        $environment = new Environment();
        $destroy = $environment->find($id);
        if (!$destroy)
            return redirect()->route('environment.index', ['line' => request('line')])->with('error', trans('returns.not_exist'));

        if ($destroy->delete())
            return redirect()->route('environment.index', ['line' => request('line')])->with('success', trans('returns.success_destroy', ['msg' => 'Ambiente']));
        else {
            return redirect()->route('environment.index', ['line' => request('line')])->with('error', trans('returns.error_destroy', ['msg' => 'Ambiente']));
        }
    }
}
