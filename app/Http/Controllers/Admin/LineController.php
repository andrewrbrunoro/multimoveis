<?php

namespace App\Http\Controllers\Admin;

use App\Environment;
use App\Http\Requests\LineRequest;
use App\Line;
use App\Product;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LineController extends Controller
{

    public function index()
    {
        $rows = Line::paginate(15);
        return view('admin.line.index', compact('rows'));
    }

    public function create()
    {
        session(['path' => Carbon::now()->timestamp]);
        return view('admin.line.create');
    }

    public function store(LineRequest $lineRequest, Line $line)
    {
        $lineRequest->merge(['slug' => str_slug($lineRequest->get('name')), 'descriptions' => json_encode($lineRequest->get('descriptions'))]);
        $new_line = $line->create($lineRequest->except(['environment']));
        if (!$new_line)
            return redirect()
                ->back()
                ->with('error', trans('returns.error_create', ['msg' => 'a Linha '. $lineRequest->get('name')]));

        if ($lineRequest->has('environments')) {
            $environment = new Environment();
            $environment->create_customized($lineRequest->get('environments'), $new_line->id);
        }

        if ($lineRequest->has('modules')) {
            $product = new Product();
            $product->create_customized(array_to_json($lineRequest->get('modules')));
        }

        return redirect()
            ->route('line.index')
            ->with('success', trans('returns.success_create', ['msg' => 'Linha '.$lineRequest->get('name'), 'url' => route('line.edit', $lineRequest->get('slug'))]));
    }

    public function edit($id)
    {
        $edit = Line::where('slug', '=', $id)->first();
        if (!$edit)
            return redirect()
                ->back()
                ->with('error', trans('returns.not_exist'));

        if ($edit->descriptions) {
            $edit->descriptions = json_decode($edit->descriptions, true);
        }


        return view('admin.line.edit', compact('edit'));
    }

    public function update($id, LineRequest $lineRequest)
    {
        $edit = Line::where('slug', '=', $id)->first();
        if (!$edit)
            return redirect()
                ->back()
                ->with('error', trans('returns.not_exist'));

        $lineRequest->merge(['slug' => str_slug($lineRequest->get('name')), 'descriptions' => json_encode($lineRequest->get('descriptions'))]);
        $edit->update($lineRequest->except(['environment']));

        $environment = new Environment();
        if ($lineRequest->has('environments')) {
            $environment->create_customized($lineRequest->get('environments'), $edit->id);
        }

        if ($lineRequest->has('itemsRemove')) {
            $environment->remove_env(explode(',',$lineRequest->get('itemsRemove')), $edit->id);
        }

        return redirect()
            ->route('line.index')
            ->with('success', trans('returns.success_update', ['msg' => 'Linha '.$lineRequest->get('name')]));
    }

    public function destroy($id)
    {
        $destroy = Line::where('slug', '=', $id);
        if (!$destroy->first())
            return redirect()
                ->back()
                ->with('error', trans('returns.not_exist'));

        $data = $destroy->first();

        if ($destroy->delete())
            return redirect()
                ->route('line.index')
                ->with('success', trans('returns.success_destroy', ['msg' => 'A Linha '. $data->name]));
        else {
            return redirect()
                ->route('line.index')
                ->with('error', trans('returns.error_destroy', ['msg' => 'a linha '. $data->name]));
        }
    }
    
}
