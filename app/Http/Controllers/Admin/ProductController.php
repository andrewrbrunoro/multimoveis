<?php

namespace App\Http\Controllers\Admin;

use App\Grid;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{

    public function index()
    {
        $rows = Product::paginate(15);
        return view('admin.product.index', compact('rows'));
    }

    public function create()
    {
        return view('admin.product.create');
    }

    public function store(Request $request, Product $product)
    {
        $request->merge(['name' => json_encode($request->get('name')), 'descriptions' => json_encode($request->get('descriptions'))]);
        $new_product = $product->create($request->except(['grids']));
        if (!$new_product)
            return redirect()->back()->with('error', trans('returns.error_create', ['msg' => 'o Módulo']));

        if ($request->has('grids')) {
            $grid = new Grid();
            $grid->save_grids($request->get('grids'), $new_product->id);
        }

        return redirect()
            ->route('product.index')
            ->with('success', trans('returns.success_create', ['msg' => 'o Módulo']));
    }

    public function edit($id)
    {
        $edit = Product::where('id', '=', $id)
            ->first();
        if (!$edit)
            return redirect()
                ->back()
                ->with('error', trans('returns.not_exist'));

        if ($edit->name) {
            $edit->name = json_decode($edit->name, true);
        }

        if ($edit->descriptions) {
            $edit->descriptions = json_decode($edit->descriptions, true);
        }

        return view('admin.product.edit', compact('edit'));
    }

    public function update($id, Request $request)
    {
        $edit = Product::where('id', '=', $id)->first();
        if (!$edit)
            return redirect()
                ->back()
                ->with('error', trans('returns.not_exist'));

        $request->merge(['name' => json_encode($request->get('name')), 'descriptions' => json_encode($request->get('descriptions'))]);
        $edit->update($request->except(['grids']));

        $grid = new Grid();
        if ($request->has('grids')) {
            $grid->save_grids($request->get('grids'), $edit->id);
        }

        if ($request->has('itemsRemove')) {
            $grid->remove_env(explode(',',$request->get('itemsRemove')), $edit->id);
        }

        return redirect()
            ->route('product.index')
            ->with('success', trans('returns.success_update', ['msg' => 'Módulo']));
    }

    public function destroy($id)
    {

    }

}
