<?php

namespace App\Http\Controllers\Admin;

use App\Events\ImageEvent;
use App\Http\Requests\PostRequest;
use App\Post;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BlogController extends Controller
{

    public function index()
    {
        $rows = Post::orderBy('created_at', 'desc')->paginate(15);
        return view('admin.blog.index', compact('rows'));
    }

    public function create()
    {
        return view('admin.blog.create');
    }

    public function store(PostRequest $postRequest, Post $post)
    {
        $postRequest->merge(['slug' => str_slug($postRequest->get('title'))]);

        if ($postRequest->get('status') == '')
            $postRequest->merge(['status' => intval(0)]);

        event(new ImageEvent($postRequest, ['path' => '/assets/post/', 'sizes' => [['w' => 760], ['w' => 100, 'h' => 100]]], 'upload'));
        if (!$post->create($postRequest->all()))
            return redirect()
                ->back()->with('error', trans('returns.error_create', ['msg' => 'Novidade']));
        return redirect()
            ->route('blog.create')
            ->with('success', trans('returns.success_create', ['url' => route('blog.edit', $postRequest->get('slug')), 'msg' => 'Novidade '. $postRequest->get('title')]));
    }

    public function edit($id, Post $post)
    {
        $edit = $post->where('slug', '=', $id)
            ->first();
        if (!$edit)
            return redirect()
                ->route('blog.index')->with('error', trans('returns.not_exist'));

        return view('admin.blog.edit', compact('edit'));
    }

    public function update($id, PostRequest $postRequest)
    {
        $edit = Post::where('slug', '=', $id)
            ->first();
        if (!$edit)
            return redirect()
                ->route('blog.index')->with('error', trans('returns.not_exist'));

        if ($postRequest->hasFile('upload'))
            event(new ImageEvent($postRequest, ['path' => '/assets/post/', 'sizes' => [['w' => 760], ['w' => 100, 'h' => 100]]], 'upload'));

        if ($postRequest->get('status') == '')
            $postRequest->merge(['status' =>  intval(0)]);

        if (!$edit->update($postRequest->all()))
            return redirect()
                ->back()->with('error', trans('returns.error_update', ['msg' => 'Novidade']));

        return redirect()
            ->route('blog.edit', $edit->slug)
            ->with('success', trans('returns.success_update', ['msg' => 'Novidade '. $postRequest->get('title')]));
    }

    public function destroy($id)
    {
        $destroy = Post::where('slug', '=', $id);
        if (!$destroy->first())
            return redirect()
                ->route('blog.index')->with('error', trans('returns.not_exist'));

        if (!$destroy->delete())
            return redirect()
                ->route('blog.index')->with('error', trans('returns.error_destroy', ['msg' => 'Novidade']));

        return redirect()
            ->route('blog.index')
            ->with('success', trans('returns.success_destroy', ['msg' => 'Novidade']));
    }

}
