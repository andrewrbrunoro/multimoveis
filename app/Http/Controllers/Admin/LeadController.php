<?php

namespace App\Http\Controllers\Admin;

use App\Lead;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LeadController extends Controller
{

    public function index()
    {
        $rows = Lead::orderBy('closed', 'desc')->paginate(15);
        return view('admin.lead.index', compact('rows'));
    }

    public function store()
    {

    }

    public function update($id)
    {
        $update = Lead::find($id);
        if (!$update)
            return redirect()
                ->back()
                ->with('error', trans('returns.not_exist'));

        if (!$update->update(['closed' => !$update->closed]))
            return redirect()
                ->back()
                ->with('error', trans('returns.error_update', ['msg' => 'o status o lead']));

        return redirect()
            ->route('lead.index')
            ->with('success', trans('returns.success_update', ['msg' => 'o lead ' . $update->name]));
    }

}
