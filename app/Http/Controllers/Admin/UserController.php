<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\UserRequest;
use App\User;
use App\UserAddress;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{

    public function index(User $user)
    {
        if (request()->has('name'))
            $user = $user->where('name', 'like', '%' . request('name') . '%');
        if (request()->has('email'))
            $user = $user->where('email', 'like', '%' . request('email') . '%');
        $rows = $user->orderBy('created_at', 'desc')->paginate(20);
        return view('admin.user.index', compact('rows'));
    }

    public function create()
    {
        return view('admin.user.create');
    }

    public function store(UserRequest $userRequest, User $user)
    {
        $new_user = $user->create($userRequest->except(['addresses', 'password_confirmation']));
        if (!$new_user) {
            return redirect()
                ->back()
                ->with('error', trans('returns.error_create', ['msg' => 'o usuário ' . $userRequest->get('name')]));
        } else {
            if ($userRequest->has('addresses')) {
                $errors = $user->save_addresses(array_to_json($userRequest->get('addresses')), $new_user->id);
                if ($errors->count()) {
                    return redirect()
                        ->back()
                        ->withErrors($errors);
                }
            }
        }
        return redirect()
            ->back()
            ->with('success', trans('returns.success_create', ['msg' => 'O usuário <strong>' . $userRequest->get('name') . '</strong>', 'url' => route('user.edit', $userRequest->get('email'))]));
    }

    public function edit($id)
    {
        $user = User::with('UserAddress')->where('email', '=', $id)->first();
        if (!$user)
            return redirect()
                ->back()
                ->with('error', trans('returns.not_exit'));
        return view('admin.user.edit', compact('user'));
    }

    public function update($id, UserRequest $userRequest)
    {
        $userModel = new User();
        $user = $userModel->where('email', '=', $id)->first();
        if (!$user)
            return redirect()
                ->route('user.index')
                ->with('error', trans('returns.not_exist'));
        else {
            $update_user = $user->update($userRequest->except(['addresses']));
            if ($userRequest->has('addresses')) {
                if (!$update_user) {
                    return redirect()
                        ->back()
                        ->with('error', trans('returns.error_update', ['msg' => 'o usuário ' . $userRequest->get('name')]));
                } else {
                    $errors = $userModel->save_addresses(array_to_json($userRequest->get('addresses')), $user->id);
                    if ($errors->count()) {
                        return redirect()
                            ->back()
                            ->withErrors($errors);
                    }
                }
            }
            if ($userRequest->has('remove_addresses')) {
                $user_addresses = new UserAddress();
                $errors = $user_addresses->remove_addresses(explode(',', $userRequest->get('remove_addresses')), $user->id);
                if ($errors->count()) {
                    return redirect()
                        ->back()
                        ->withErrors($errors);
                }
            }
        }
        return redirect()
            ->route('user.index')
            ->with('success', trans('returns.success_update', ['msg' => 'O usuário <strong>' . $userRequest->get('name') . '</strong>']));
    }

    public function destroy($id)
    {
        $userModel = new User();
        $user = $userModel->where('email', '=', $id);
        $user_data = $user->first();
        if (!$user->first())
            return redirect()
                ->route('user.index')
                ->with('error', trans('returns.not_exist'));
        else {
            if (!$user->delete()) {
                return redirect()
                    ->route('user.index')
                    ->with('error', trans('returns.error_destroy', ['msg' => 'o usuário ' . $user_data->name]));
            }
            $user_addresses = UserAddress::where('user_id', '=', $user_data->id);
            if ($user_addresses->get()->count()) {
                if (!$user_addresses->delete()) {
                    return redirect()
                        ->route('user.index')
                        ->with('error', trans('returns.error_destroy', ['msg' => 'o usuário ' . $user_data->name]));
                }
            }
        }
        return redirect()
            ->route('user.index')
            ->with('success', trans('returns.success_destroy', ['msg' => 'O usuário ' . $user_data->name]));
    }

}
