<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\MessageBag;

class UserAddress extends Model
{

    use SoftDeletes;

    protected $fillable = [
        'user_id', 'zip_code', 'street', 'street_number',
        'district', 'city', 'state', 'country', 'geo_location'
    ];

    public function remove_addresses(array $array, $user_id)
    {
        $error = new MessageBag();
        foreach ($array as $item) {
            $exist = $this->where('user_id', '=', $user_id)->where('id', '=', $item);
            if ($exist->count()) {
                if (!$exist->delete()) {
                    $exist = $exist->first();
                    $error->add($exist->zip_code, 'Endereço do CEP: '. $exist->zip_code.', não foi possível remove. Tente novamente.');
                }
            }
        }
        return $error;
    }

}
