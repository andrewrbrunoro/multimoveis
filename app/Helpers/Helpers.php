<?php
use Carbon\Carbon;

if (!function_exists('json_to_name')) {
    function json_to_name ($json, $key) {
        $json = json_decode($json, true);
        return $json[$key];
    }
}

if (!function_exists('array_to_json')) {
    function array_to_json(array $array)
    {
        $decode_array = [];
        foreach ($array as $item) {
            $json = json_decode($item, true);
            if (is_array($json)) {
                $decode_array[] = $json;
            }
        }
        return $decode_array;
    }
}

if (!function_exists('setting')) {
    function setting($key, $json = false)
    {
        $settings = new \App\Setting();
        if (env('APP_DEBUG') == true) {
            $settings->loadSettings(true);
        }

        $settings = $settings->loadSettings();
        if (isset($settings[$key])) {
            return $json ? json_decode($settings[$key]) : $settings[$key];
        }
        if (env('DEBUG') == true)
            return 'Configurações não encontrada ' . $key;
        return '';
    }
}

if (!function_exists('dateToSql')) {
    function dateToSql($date, $format = 'd/m/Y', $out = 'Y-m-d')
    {
        if (!empty($date) && $date != '0000-00-00')
            return Carbon::createFromFormat($format, $date)->format($out);
        else {
            return '';
        }
    }
}


if (!function_exists('sqlToDate')) {
    function sqlToDate($date, $format = 'Y-m-d H:i:s', $out = 'd/m/Y H:i:s')
    {
        if (!empty($date) && $date != '0000-00-00')
            return Carbon::createFromFormat($format, $date)->format($out);
        else {
            return '';
        }
    }
}
