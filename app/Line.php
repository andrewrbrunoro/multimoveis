<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Line extends Model
{

    use SoftDeletes;

    protected $fillable = [
        'name', 'slug', 'descriptions'
    ];

    public function getDescriptionsJsonAttribute()
    {
        return json_decode($this->descriptions, true);
    }

    public function not_repeat_slug($slug)
    {
        $search = $this->where('slug', '=', $slug)->count();
        if ($search)
            return $slug . '-01';
        else {
            return $slug;
        }
    }

    public function Environment()
    {
        return $this->hasMany('App\Environment', 'line_id');
    }

}
