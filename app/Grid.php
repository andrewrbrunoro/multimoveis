<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\File;
use Illuminate\Support\MessageBag;

class Grid extends Model
{

    use SoftDeletes;

    protected $fillable = [
        'product_id', 'color_id',
        'reference', 'upload'
    ];

    public function getNameJsonAttribute()
    {
        return json_decode($this->name);
    }

    public function getUploadJsonAttribute()
    {
        return json_decode($this->upload);
    }

    public function save_grids(array $grids, $product_id)
    {
        if (count($grids)) {
            $error = new MessageBag();
            foreach ($grids as $grid) {

                $save = json_decode($grid, true);

                $this->upload($save['upload']);

                $save['upload']['path'] = url('/assets/environment/');
                $save['upload'] = json_encode($save['upload']);
                $save['product_id'] = $product_id;
                $find = $this->where('id', '=', $save['id'])->first();
                unset($save['id']);
                if (!$find) {
                    if (!$this->create($save)) {
                        $error->add($save['reference'], 'Falha ao criar variação do módulo');
                    }
                } else {
                    if (!$this->update($save)) {
                        $error->add($save['reference'], 'Falha ao atualizar a variação do módulo');
                    }
                }
            }
            return $error;
        }
    }

    private function upload($upload)
    {
        if (File::exists(public_path('/uploads/tmp-image/' . $upload['name']))) {
            if (!File::isDirectory(public_path('/assets/product/')))
                File::makeDirectory(public_path('/assets/product/'), 0775, true);

            File::move(public_path('/uploads/tmp-image/' . $upload['name']), public_path('/assets/product/' . $upload['name']));
            File::delete(public_path('/uploads/tmp-image/' . $upload['name']));
        }

        if (File::exists(public_path('/uploads/tmp-image/' . $upload['w']))) {
            if (!File::isDirectory(public_path('/assets/product/')))
                File::makeDirectory(public_path('/assets/product/'), 0775, true);

            File::move(public_path('/uploads/tmp-image/' . $upload['w']), public_path('/assets/product/' . $upload['w']));
            File::delete(public_path('/uploads/tmp-image/' . $upload['w']));
        }
    }

    public function remove_env($array, $id)
    {
        foreach ($array as $item) {
            $exist = $this->where('id', '=', $item)
                ->where('product_id', '=', $id);
            if ($exist->count()) {
                $exist->delete();
            }
        }
    }

}
