<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\File;
use Illuminate\Support\MessageBag;

class Environment extends Model
{

    use SoftDeletes;

    protected $fillable = [
        'line_id', 'color_id', 'name',
        'reference', 'upload'
    ];

    public function getPrettyAttribute()
    {
        $ret = '';
        if ($this->name) {
            $json = json_decode($this->name);
            foreach ($json as $item) {
                $ret .= $item . ', ';
            }
            $ret = substr($ret, 0, strlen($ret) - 2);
        }
        return $ret;
    }

    public function getNameJsonAttribute()
    {
        return json_decode($this->name);
    }

    public function getUploadJsonAttribute()
    {
        return json_decode($this->upload);
    }

    public function create_customized(array $json, $id)
    {
        $error = new MessageBag();
        foreach ($json as $item) {

            $save = json_decode($item, true);

            $this->upload($save['upload']);

            $save['name'] = json_encode($save['name']);
            $save['upload']['path'] = url('/assets/environment/');
            $save['upload'] = json_encode($save['upload']);
            $save['line_id'] = $id;

            $find = Environment::where('id', '=', $save['id'])->first();
            unset($save['id']);
            if (!$find) {
                if (!$this->create($save)) {
                    $error->add($save['reference'], 'Não foi possível criar o ambiente REF: '. $save['reference']);
                }
            } else {
                if (!$find->update($save)) {
                    $error->add($save['reference'], 'Não foi possível atualizar o ambiente REF: '. $save['reference']);
                }
            }
        }
        return $error;
    }

    private function upload($upload)
    {
        if (!File::isDirectory(public_path('/assets/environment/')))
            File::makeDirectory(public_path('/assets/environment/'), 0775, true);

        foreach ($upload as $item) {
            if (File::exists(public_path('/uploads/tmp-image/' . $item))) {
                File::move(public_path('/uploads/tmp-image/' . $item), public_path('/assets/environment/' . $item));
                File::delete(public_path('/uploads/tmp-image/' . $item));
            }
        }
    }

    public function remove_env($array, $id)
    {
        foreach ($array as $item) {
            $exist = $this->where('id', '=', $item)
                ->where('line_id', '=', $id);
            if ($exist->count()) {
                $exist->delete();
            }
        }
    }

    public function Color()
    {
        return $this->belongsTo('App\Color', 'color_id');
    }

}
