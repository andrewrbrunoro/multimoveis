<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\MessageBag;

class User extends Authenticatable
{
    use Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'last_name', 'email',
        'phone', 'password', 'type'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function setPasswordAttribute()
    {
        if (request()->has('password'))
            $this->attributes['password'] = bcrypt(request('password'));
        else {
            $request = request()->all();
//            if (isset($request['password']))
//                unset(request()->all()['password']);
        }
    }

    public function UserAddress()
    {
        return $this->hasMany('App\UserAddress', 'user_id');
    }

    /**
     * @param array $addresses
     * @param $user_id
     * @return MessageBag
     */
    public function save_addresses(array $addresses, $user_id)
    {
        $error = new MessageBag();
        $user_address = new UserAddress();
        foreach ($addresses as $key => $address) {
            $address_exist = $user_address->where('user_id', '=', $user_id)->where('zip_code', '=', $address['zip_code'])->first();
            $return = $address_exist ? $address_exist->update($address + ['user_id' => $user_id]) : $user_address->create($address + ['user_id' => $user_id]);
            if (!$return)
                $error->add($address['zip_code'], 'Não foi possível salvar o endereço do CEP: '. $address['zip_code'].'.');
        }
        return $error;
    }

}
