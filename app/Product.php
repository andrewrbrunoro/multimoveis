<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{

    use SoftDeletes;

    protected $fillable = [
        'environment_id', 'name', 'descriptions',
        'height', 'width', 'depth', 'video'
    ];


    public function Environment()
    {
        return $this->belongsTo('App\Environment', 'environment_id');
    }

    public function Grid()
    {
        return $this->hasMany('App\Grid', 'product_id');
    }

    public function create_customized(array $array)
    {
        $grid = new Grid();
        $env = new Environment();
        foreach ($array as $item) {
            $item['environment_id'] = $item['environment'];
            $item['name'] = json_encode($item['name']);
            $item['descriptions'] = json_encode($item['descriptions']);

            $env_exist = $env->where('reference', '=', $item['environment'])->first();
            if ($env_exist)
                $item['environment_id'] = $env_exist->id;
            else {
                $item['environment_id'] = '';
            }

            $f = collect($item);

            $create = $this->create([
                'name' => $f->get('name'),
                'environment_id' => $f->get('environment_id'),
                'descriptions' => $f->get('descriptions'),
                'height' => $f->get('height'),
                'width' => $f->get('width'),
                'depth' => $f->get('depth')
            ]);
            if (count($item['grids'])) {
                $make = [];
                foreach ($item['grids'] as $grids) {
                    $make[] = json_encode($grids);
                }
                $grid->save_grids($make, $create->id);
            }
        }
    }

    public function setHeightAttribute($value)
    {
        $this->attributes['height'] = !empty($value) ? $value : '0.00';
    }

    public function setWidthAttribute($value)
    {
        $this->attributes['width'] = !empty($value) ? $value : '0.00';
    }

    public function setDepthAttribute($value)
    {
        $this->attributes['depth'] = !empty($value) ? $value : '0.00';
    }

    public function setEnvironmentIdAttribute($value)
    {
        $this->attributes['environment_id'] = !empty($value) ? $value : 0;
    }

}
