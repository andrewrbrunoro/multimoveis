<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Color extends Model
{

    use SoftDeletes;

    protected $fillable = [
        'name', 'value'
    ];

    public function getSelectAttribute()
    {
        if (json_decode($this->name, true)) {
            $return = '';
            foreach (json_decode($this->name) as $name) {
                $return .= $name->name.', ';
            }
            $return = substr($return, 0, strlen($return) - 2);
            return $return;
        }
        return $this->name;
    }

}
